extends Node

const HEX_SIZE := 60
const DIRECTIONS := [
	Vector2i(1, 0),
	Vector2i(1, 1),
	Vector2i(-1, 1),
	Vector2i(-1, 0),
	Vector2i(-1, -1),
	Vector2i(1, -1)
]

var CENTER_POSITION := Vector2(
	ProjectSettings.get("display/window/size/viewport_width") / 2,
	ProjectSettings.get("display/window/size/viewport_width") / 2
)
