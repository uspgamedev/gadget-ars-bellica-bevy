extends Component
class_name HexCamera

#var target_hex := Vector2i() : get = get_hex

@onready var camera = $Camera3D

func get_hex() -> Vector2i:
	if is_instance_valid(camera.player_hex):
		return camera.player_hex.tile
	return MapConstants.position_to_hex(camera.target_position)

static func get_camera() -> HexCamera:
	return Simulation.get_simulation().query([HexCamera])[0].get_component(HexCamera)
