extends TextureRect

@export var entity_requester: EntityRequester

func _process(delta):
	var entity := entity_requester.request()
	
	# TODO: refactor gadget to entity
	self.texture = entity.get_component(Hand).get_gadget().get_node("Sprite3D").texture if is_instance_valid(entity) and entity.get_component(Hand).get_gadget() else null
