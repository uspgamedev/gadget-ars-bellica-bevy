extends HBoxContainer

var prototype: CanvasItem

@export var entity: EntityRequester
@export var component: Script
@export var var_name: String
@export var ratio := 1

func _ready():
	prototype = get_child(0)
	remove_child(prototype)

func _process(_delta):
	update_bar()
	
func update_bar():
	var target := entity.request()
	var count = int(round(target.get_component(component).get(var_name)/ratio)) if is_instance_valid(target) else 0
	
	if get_child_count() > count:
		for _i in get_child_count() - count:
			get_child(0).queue_free()
	elif get_child_count() < count:
		for _i in count - get_child_count():
			add_child(prototype.duplicate())
