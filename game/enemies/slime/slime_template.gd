extends Entity

@export_enum("none", "fire", "water", "air", "earth") var element := 0

@onready var body: Body = get_component(Body)

func _ready():
	match element:
		1:
			body.sprite.modulate = Color.RED
		2:
			body.sprite.modulate = Color.BLUE
		3:
			body.sprite.modulate = Color(Color.SNOW, 0.35)
		4:
			body.sprite.modulate = Color.CHOCOLATE
