extends Entity

@export_enum("none", "fire", "water", "air", "earth") var element := 0

@onready var body: Body = get_component(Body)

func _ready():
	match element:
		1:
			body.sprite.modulate = Color.RED
			%AreaAttackStrategy.color = Color.RED
			%AttributeAttackStrategy.attributes = [FireAttr.new()] as Array[Attribute]
		2:
			body.sprite.modulate = Color.BLUE
			%AreaAttackStrategy.color = Color.BLUE
			%AttributeAttackStrategy.attributes = [IceAttr.new()] as Array[Attribute]
			%ActOnTurn.turn_interval = 4
		3:
			body.sprite.modulate = Color(Color.SNOW, 0.5)
			%AreaAttackStrategy.color = Color.SNOW
			%ActOnTurn.turn_interval = 1
		4:
			body.sprite.modulate = Color.CHOCOLATE
			%AreaAttackStrategy.color = Color.CHOCOLATE
			%AreaAttackStrategy.outer_radius = 3

