extends Component3D
class_name Hand

func get_gadget() -> Gadget:
	if get_child_count() > 0:
		return get_child(0)
	return null 

func remove_gadget() -> Gadget:
	if get_child_count() > 0:
		var gadget_ = get_child(0)
		remove_child(get_child(0))
		return gadget_
	return null 

func can_put_gadget() -> bool:
	if get_child_count() > 0:
		return false
	return true
	
func put_gadget(gadget_: Gadget) -> bool:
	if get_child_count() > 0:
		return false
	add_child(gadget_)
	return true

func interact_with(target: TargetAttr):
	var action := InteractionPlugin.new_interact_action(target)

	if get_child_count() > 0:
		var gadget := get_gadget()
		for attr in gadget.get_interaction_attributes():
			action.add_attribute(attr)

	Simulation.apply_action(action)


func attack(tile: Vector2i) -> bool:
	if get_child_count() > 0:
		var gadget := get_gadget()
		return await gadget.attack(tile)
	return false
