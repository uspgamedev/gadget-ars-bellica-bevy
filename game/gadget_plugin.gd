extends Node

@export var item_prototypes : JSON

func _ready():
	ItemRegistry.register_item_prototypes(item_prototypes.data)
