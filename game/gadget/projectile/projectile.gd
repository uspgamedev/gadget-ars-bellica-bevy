extends Component3D
class_name Projectile

@export var attributes: Array[Attribute] = [RangedAttr.new()]
## Tile/turn
@export var speed := 20.
@export var angular_speed := 5.0

@export var attack_strategy : AttackStrategy = SimpleAttackStrategy.new()

@export var texture: Texture2D

@export var levels : Array[Blocking.Level] = [Blocking.Level.Ground]

signal finished


func _ready():
	if texture:
		%Sprite3D.texture = texture


func shoot(from: Vector2i, direction: Vector2i, distance: int, action: Action):
	self.global_position = MapConstants.hex_to_position(from)

	direction = MapConstants.get_direction(direction)

	var target = from + direction * distance
	var entities = Map.get_map().get_entities_on_line(from, direction, distance).filter(Blocking.filter_by_level.bind(levels))
	
	var hex = Hex.new()

	if not entities.is_empty():
		target = entities[0].get_component(Hex).tile
		hex.tile = target - direction
	else:
		hex.tile = target
		
	get_entity().add_child(hex)
	
	var tween = create_tween()
	
	tween.tween_property(
		self,
		"global_position",
		MapConstants.hex_to_position(target),
		distance / speed
	)
	
#	await tween.finished
#	tween.finished.connect(func(): 
#		Simulation.apply_action(CombatPlugin.new_kill_action(TargetAttr.new(get_entity())))
#	)
	await tween.finished
	Simulation.apply_action(CombatPlugin.new_kill_action(TargetAttr.new(get_entity())))
	
#	# TODO: remove this and move this method to a rule?
#	action.add_attribute(HexAttr.new(target))
	
	attack_strategy.attack(Action.new_action([HexAttr.new(target), action.get_attribute(FromAttr), AttackAttr.new()], ""))

	
