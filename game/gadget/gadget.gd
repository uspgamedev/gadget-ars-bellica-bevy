extends Node
class_name Gadget

@export var interaction_attributes: Array[Attribute] = []

@export var item_id := "wrench" 

func attack(tile : Vector2i) -> bool:
	var attack_strategy: AttackStrategy = get_attack_strategy()
		
	for child in get_children():
		if child is Modifier:
			var modifier_strategy = child.get_attack_strategy()
			if modifier_strategy:
				attack_strategy.combine_strategy(modifier_strategy)
	
	add_child(attack_strategy)
	
	var attack_result := await attack_strategy.attack(Action.new_action([HexAttr.new(tile), FromAttr.new(get_parent().get_entity()), AttackAttr.new()], ""))
	
	remove_child(attack_strategy)
	
	return attack_result

func search(tile : Vector2i) -> Array:
	var map := Map.get_map()
	var search_strategy = get_children().filter(func(x): return x is SearchStrategy)[0] as SearchStrategy
	if search_strategy:
		return search_strategy.search(map, tile)
	return []

func get_attack_strategy() -> AttackStrategy:
	for child in get_children():
		if child is AttackStrategy:
			return child.duplicate()
	return null
	
func get_interaction_attributes() -> Array[Attribute]:
	var attributes : Array[Attribute] = []
	for child in get_children():
		if child is Modifier:
			attributes.append_array(child.get_attributes())
	
	attributes.append_array(interaction_attributes)
	
	return attributes

# TODO: generalize this
#func serialize() -> Dictionary:
func as_item() -> Item:
	var dict = { "modifiers": []}
	
	for mod in get_modifiers():
		dict.modifiers.push_back(mod.item_id)
	
	return Item.new(item_id, dict)
	
func get_modifiers() -> Array[Modifier]:
	var modifiers : Array[Modifier] = []
	
	for child in get_children():
		if child is Modifier:
			modifiers.push_back(child)
			
	return modifiers
	
	
	
	
