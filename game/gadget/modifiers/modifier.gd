extends Node
class_name Modifier

@export var attributes: Array[Attribute] = []
@export var item_id := "blade"

func get_attributes() -> Array[Attribute]:
	return attributes

func get_attack_strategy() -> AttackStrategy:
	for child in get_children():
		if child is AttackStrategy:
			return child.duplicate()
	return null
