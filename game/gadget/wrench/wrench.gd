extends Gadget
class_name Wrench

func interact(target: Entity) -> bool:
	push_error("Interface method")
	return false

func attack(target: Vector2i) -> bool:
	push_error("Interface method")
	return false
