extends Rule

var target := TargetAttr.new()
var spin := SpinAttr.new()
var amount := AmountAttr.new()


func _apply(_simulation: Simulation):
	var entity = target.entity
	var hex = entity.get_component(Hex)
	var aim = entity.get_component(Aim)
	
	if aim and hex:
		var diff : Vector2 = aim.tile - hex.tile
		diff = diff.rotated(amount.value)
		aim.tile = hex.tile + diff
		
