extends Rule

var target := TargetAttr.new()
var interact := InteractAttr.new()
var mod := SlashAttr.new()

func _apply(_simulation: Simulation):
	if target.entity.get_component(Wood):
		Simulation.apply_action(CombatPlugin.new_kill_action(TargetAttr.new(target.entity)))
		
