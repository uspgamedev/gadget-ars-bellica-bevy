class_name Aim
extends Component


@onready var entity := get_entity()

var tile

var redo = false

var parent_tile

func _ready():
	if not redo:
		redo = true
		return
	redo = false
	var parent = EntityUtil.get_entity(entity)
	var parent_hex: Hex = parent.get_component(Hex)
	parent_tile = parent_hex.tile
	
	parent_hex.hex_changed.connect(
		func(x):
			var hex = entity.get_component(Hex)
			hex.tile += x - parent_tile 
			hex.update_position = false
			hex.update_tile()
			hex.update_position = true
			parent_tile = x
	)
	
	(
		func():
			var hex = entity.get_component(Hex)
			
			hex.global_position = MapConstants.hex_to_position(tile + parent_hex.tile)
			
			entity.get_component(Hex).update_position = false
			entity.get_component(Hex).update_tile()
			entity.get_component(Hex).update_position = true
			
	).call_deferred()
	
