@tool
extends Entity

@export var tile: TileRequester = DefaultTileRequester.new(self)

func _ready():
	if not Engine.is_editor_hint():
		get_component(Aim).tile = tile.request() - EntityUtil.get_entity(self).get_component(Hex).tile
	
