extends Component
class_name Bridge

func _on_place_wood_placed_all_items():
	fix()
	
func fix():
	var body: Body = get_entity().get_component(Body)
	body.play("fixed")
	add_child(Blocking.new(8))
	
