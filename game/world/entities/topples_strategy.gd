extends InteractionStrategy

@export var entity_requester: EntityRequester = DefaultEntityRequester.new(self)
@export var bridge_scene: PackedScene

func interact(action: Action):
	var push = action.get_attribute(PushAttr)
	var from = action.get_attribute(FromAttr)
	
	if not push or not from:
		return
	
	var entity = entity_requester.request()
	
	if entity.get_component(Blocking):
		entity.remove_component(Blocking)
	
	var direction = (entity.get_component(Hex).tile - from.entity.get_component(Hex).tile)
	
	var bridge = bridge_scene.instantiate()
	bridge.get_component(Hex).tile  = direction + entity.get_component(Hex).tile
	Simulation.get_simulation().add_child(bridge)
	
	bridge.get_component(Bridge).fix()
	
	
	

