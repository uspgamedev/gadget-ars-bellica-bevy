extends Component


@export var up := false
@export var value := 0
@export_enum("diamonds", "spades", "hearts", "clubs") var suit := "diamonds" 

@onready var entity := get_entity()
@onready var body: Body = entity.get_component(Body)

func _ready():
	update()

func update():
	if not up:
		body.sprite.animation = "default"
		return
	
	body.sprite.animation = suit
	
	body.sprite.frame = value % 13
