extends Resource
class_name GroundType

@export var scene: PackedScene
@export var icon: Texture2D

static func instantiate(type: GroundType) -> Node:
	return type.scene.instantiate()
