@tool
extends TileMap

@export var update_tileset := false : set = set2
@export var generate_ground := false : set = set1
@export var id_to_prototype := {}

@export var ground_types: Array[GroundType] = []

func set1(_x: bool):
	if Engine.is_editor_hint() and _x:
		update_ground()
		
func set2(_x: bool):
	if Engine.is_editor_hint() and _x:
		update_tile_set()

func _ready():
	if not Engine.is_editor_hint():
		self.visible = false


func update_tile_set():
	tile_set = TileSet.new()
	tile_set.tile_shape = TileSet.TILE_SHAPE_HEXAGON
	for i in len(ground_types):
		var ground_type = ground_types[i] 
		var texture = ground_type.icon as Texture2D

		texture.get_size()

		var atlas := TileSetAtlasSource.new()
		atlas.texture = texture
#		atlas.texture_region_size = texture.get_size()
		atlas.use_texture_padding = false
		for x in round(texture.get_size().x/16): 
			for y in round(texture.get_size().y/16):
				atlas.create_tile(Vector2i(x, y))
		tile_set.add_source(atlas, i)

		if !id_to_prototype.get(i):
			id_to_prototype[i] = ground_type.scene
			
func update_tiles():
	return

func update_ground():
	if not is_instance_valid(get_tree()):
		return
	
	for child in get_children():
		child.queue_free()
	
	for tile in get_used_cells(0):
		var id = get_cell_source_id(0, tile)
				
		var ground_item = GroundType.instantiate(ground_types[id])
		ground_item.request_ready()
		if tile.y % 2 == 0:
			ground_item.position = MapConstants.hex_to_position(Vector2i(tile.x*2, tile.y+1))
		else:
			ground_item.position = MapConstants.hex_to_position(Vector2i(tile.x*2+1, tile.y+1))
		
		ground_item.position.x += MapConstants.D/2
#		ground_item.position = MapConstants.hex_to_position(ground_item.tile)
#		ground_item.prototype_id = id

#		if get_parent():
		self.add_child.bind(ground_item).call_deferred()
		ground_item.set_owner.bind(get_tree().get_edited_scene_root()).call_deferred()
#			(func(): ground_item.name = entity.name + str(ground_item.get_index())).call_deferred()
		
