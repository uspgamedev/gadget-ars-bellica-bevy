@tool
extends Sprite2D

@export var enabled := false
@export var blocking := false

@export var tile := Vector2i() : get = get_tile, set = set_tile

@onready var sprite3d : Sprite3D = %GroundTile3D

func get_tile() -> Vector2i:
	return MapConstants.position_2d_to_hex(self.position)
	
func set_tile(new_tile: Vector2i):
	self.position = MapConstants.hex_to_position_2d(new_tile)
	tile = new_tile

func _ready():
	init_sprite3d()
	if Engine.is_editor_hint():
		return
		
	remove_child.bind(%Entity).call_deferred()
	
	if blocking:
		var hex_component = Hex.new()
		hex_component.tile = get_tile()
		%Entity.add_child.bind(Blocking.new(4)).call_deferred()
		%Entity.add_child.bind(hex_component).call_deferred()
		
	add_sibling.bind(%Entity).call_deferred()
	queue_free()
		

	

func _process(_delta):
	if Engine.is_editor_hint() and enabled:
		init_sprite3d()
		
func init_sprite3d():
	var new_position = MapConstants.hex_to_position(tile)
	self.position = MapConstants.hex_to_position_2d(tile)
		
	sprite3d.position.x = new_position.x
	sprite3d.position.z = new_position.z
	
	sprite3d.rotation.y = -self.rotation
	sprite3d.scale.x = self.scale.x/0.125 * 46.85
	sprite3d.scale.z = self.scale.y/0.125 * 54.15

	sprite3d.texture = self.texture
	sprite3d.hframes = self.hframes
	sprite3d.vframes = self.vframes
	sprite3d.frame = self.frame
	
	sprite3d.flip_h = self.flip_h
	sprite3d.flip_v = self.flip_v
