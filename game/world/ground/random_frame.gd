@tool
extends Sprite3D

func _ready():
	if !Engine.is_editor_hint():
		visible = randf() < 0.35
		frame = randi_range(0, (hframes-1) * (vframes-1) + 1)
	else:
		visible = false
