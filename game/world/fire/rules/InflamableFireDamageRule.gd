extends Rule

var fire := FireAttr.new()
var health := HealthAttr.new()
var amount := AmountAttr.new()
var target := TargetAttr.new()


func _modify(_simulation: Simulation):
	if target.entity:
		var inflamable = target.entity.get_component(Inflamable) as Inflamable
		if inflamable:
			amount.value *= inflamable.fire_damage_multiplier
