extends Rule

var turn := TurnAttr.new()

func _apply(simulation: Simulation):
	var entities := simulation.query([Frozen])

	for entity in entities:
		var frozen = entity.get_component(Frozen) as Frozen
		frozen.dissipate()
