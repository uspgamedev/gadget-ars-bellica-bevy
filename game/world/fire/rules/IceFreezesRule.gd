extends Rule

var fire := IceAttr.new()
var target := TargetAttr.new()


func _apply(_simulation: Simulation):
	var entity = target.entity
	
	# TODO: Remove this
	if is_instance_valid(entity):
		var frozen = Frozen.new()
		if entity.get_component(Frozen) == null:
			entity.set_component(frozen)
