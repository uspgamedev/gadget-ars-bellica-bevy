extends Rule

var fire := FireAttr.new()
var target := TargetAttr.new()


func _apply(_simulation: Simulation):
	var entity = target.entity
	
	# TODO: Remove this
	if is_instance_valid(entity):
		if entity.get_component(Inflamable):
			var burn_mark = BurnMark.new()
			burn_mark.damage = entity.get_component(Inflamable).burn_damage
			entity.set_component(burn_mark)
