extends Rule

var act := ActAttr.new()
var target := TargetAttr.new()

func _check(_simulation: Simulation):
	if target.entity.get_component(Frozen):
		prevent()
		target.entity.get_component(Frozen).dissipate()
