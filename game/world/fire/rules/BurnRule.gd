extends Rule

var turn := TurnAttr.new()


func _apply(simulation: Simulation):
	var entities := simulation.query([BurnMark])

	for entity in entities:
		var burn_mark = entity.get_component(BurnMark) as BurnMark
		Simulation.apply_action(
			(
				CombatPlugin
					.new_change_health_action(TargetAttr.new(entity), AmountAttr.new(-burn_mark.damage))
					# TODO: avoid to initiate burn again
#					.add_attribute(FireAttr.new())
			)
		)
		burn_mark.dissipate()
