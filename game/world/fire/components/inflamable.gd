extends Component
class_name Inflamable

## Burn damagge per turn if is on fire
@export var burn_damage := 10
@export var fire_damage_multiplier := 2.0
