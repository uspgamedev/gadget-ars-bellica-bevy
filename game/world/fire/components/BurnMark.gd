extends Component
class_name BurnMark

var damage := 20

@export var accumulation := 2

@onready var body := get_entity().get_component(Body) as Body

func _ready():
	update_color()
	body.info1.visible = true
	
func _exit_tree():
	body.info1.visible = false


func accumulate():
	accumulation += 1
	update_color()


func dissipate():
	accumulation -= 1
	update_color()
	if accumulation <= 0:
		get_entity().remove_component(BurnMark)


func get_accumulation() -> int:
	return accumulation


func update_color():
	if get_entity() == null:
		return

#	var body := get_entity().get_component(Body) as Body
	# TODO: create visual effects api for body
#	if body:
#		var color := (
#			(Color(1.0, 1.0, 1.0) + Color(1.0 * accumulation, 0.5, 0.5)) / (1 + accumulation)
#		)
#		color.a = 1.0
#		body.set_modulate(color)
