extends InteractionStrategy

@export var entity: EntityRequester = DefaultEntityRequester.new(self)
@export var center: TileRequester = DefaultTileRequester.new(self)
@export var angle := PI/6

func interact(_action: Action):
	if not is_instance_valid(entity):
		return
		
	var hex = entity.request().get_component(Hex)
	var relative = MapConstants.hex_to_position(hex.tile) -  MapConstants.hex_to_position(center.request())
	hex.tile = MapConstants.position_to_hex(relative.rotated(Vector3.UP, angle) + MapConstants.hex_to_position(center.request()))
	
