extends Rule

var turn := TurnAttr.new()

func _apply(simulation: Simulation):
	var entities := simulation.query([MoveOnTurn])
	
	for entity in entities:
		entity.get_component(MoveOnTurn).move()
	
