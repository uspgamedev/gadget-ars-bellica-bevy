extends MoveStrategy

func _get_new_hex() -> Vector2i:
	return MapConstants.DIRECTIONS.pick_random() as Vector2i
