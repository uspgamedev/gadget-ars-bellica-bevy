extends InputStrategy

#@export var move_range := 1

@export var from_tile: TileRequester

@onready var ap: ActionPoints = EntityUtil.get_entity(self).get_component(ActionPoints)

func _can_apply(event: InputEvent) -> bool:
	return event.is_action_pressed("move")

func _apply(target: Vector2i):
	var displacement = target - from_tile.request()
	#if MapConstants.length(displacement) > move_range:
		#return
	
	
	#if Simulation.apply_action(
	#MapPlugin.new_move_action(displacement, get_parent().get_entity())
		#):
		#await create_tween().tween_interval(0.2).finished
	
	for tile in Map.get_map().get_tile_path(from_tile.request(), target):
		displacement = tile - from_tile.request()
		
		if ap.points > 0 and  Simulation.apply_action(
			MapPlugin.new_move_action(displacement, EntityUtil.get_entity(self))
		):
			ap.points -= 1
			await create_tween().tween_interval(0.2).finished
	
	if ap.points <= 0:
		ap.points = ap.max_points
		Simulation.get_simulation().apply_action.bind(GeneralPlugin.new_advance_turn_action()).call_deferred()
			
	current_tile = null
			
var current_tile = Vector2i()
var tile_id = -1
@onready var visual_map := Map.get_map().visual
var color := Color(Color.YELLOW, 0.7)

var path = []

func get_cost(target: Vector2i) -> int:
	return Map.get_map().get_tile_path(from_tile.request(), target).size()-1


func _preview(tile: Vector2i):
	if current_tile == null or current_tile != tile:
		if tile_id != -1:
			visual_map.remove_tile(tile_id)
		
		for tile_path in path:
			visual_map.remove_tile(tile_path)	
		
		var new_color = color
		var cost = get_cost(tile)
		
		#tile_id = visual_map.add_tile(tile, new_color, str(cost), Color.YELLOW)
		
		var pat = Map.get_map().get_tile_path(from_tile.request(), tile)
		#pat.pop_back()
		pat.pop_front()
		cost = 1
		for tile_path in pat:
			if cost > ap.points:
				new_color = Color(Color.YELLOW, 0.4)
			path.push_back(visual_map.add_tile(tile_path, new_color, str(cost), Color.YELLOW))
			cost += 1
		
		if pat.is_empty():
			new_color = Color(Color.YELLOW, 0.4)
			path.push_back(visual_map.add_tile(tile, new_color, "∞", Color.YELLOW))
		current_tile = tile

func _exit_tree():
	if tile_id != -1:
		visual_map.remove_tile(tile_id)
		tile_id = -1
	for tile in path:
		visual_map.remove_tile(tile)
	current_tile = null
	
