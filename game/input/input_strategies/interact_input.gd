extends InputStrategy

@export var hand: Hand

func _can_apply(event: InputEvent) -> bool:
	return event.is_action_pressed("move")

func _apply(target: Vector2i):
	var map := Map.get_map()
	# TODO: fazer o player escolher quem atacar
	var target_entities := map.get_entities_on_hex(target)
	
	#if MapConstants.DIRECTIONS.find(target) != -1:
		#var hand := get_parent().get_entity().get_component(Hand) as Hand
	if hand:
		for e in target_entities:
			hand.interact_with(TargetAttr.new(e))

# TODO: remove duplicate code (move_input.gd)
var current_tile = Vector2i()
var tile_id = -1
@onready var visual_map := Map.get_map().visual
var color := Color(Color.GREEN, 0.7) 

func _preview(tile: Vector2i):
	if current_tile == null or current_tile != tile:
		if tile_id != -1:
			visual_map.remove_tile(tile_id)
		tile_id = visual_map.add_tile(tile, color, str(0), Color.GREEN)
		current_tile = tile
		
func _exit_tree():
	if tile_id != -1:
		visual_map.remove_tile(tile_id)
		tile_id = -1
	current_tile = null
