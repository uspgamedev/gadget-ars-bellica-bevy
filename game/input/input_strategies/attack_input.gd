extends InputStrategy

func _can_apply(event: InputEvent) -> bool:
	return event.is_action_pressed("move")

func _apply(displacement: Vector2i):
	var hand := get_parent().get_entity().get_component(Hand) as Hand
	if await hand.attack(displacement):
		Simulation.get_simulation().apply_action.bind(GeneralPlugin.new_advance_turn_action()).call_deferred()


# TODO: remove duplicate code (move_input.gd)
var current_tile = Vector2i()
var tile_id = -1
@onready var visual_map := Map.get_map().visual
var color := Color(Color.RED, 0.7) 

func _preview(tile: Vector2i):
	if current_tile == null or current_tile != tile:
		if tile_id != -1:
			visual_map.remove_tile(tile_id)
		tile_id = visual_map.add_tile(tile, color, str(0), Color.RED)
		current_tile = tile

func _exit_tree():
	if tile_id != -1:
		visual_map.remove_tile(tile_id)
		tile_id = -1
	current_tile = null
	
