extends Component
class_name MoveOnTurn

func move():
	var strategy = get_child(0) as MoveStrategy
	if strategy:
		Simulation.apply_action(MapPlugin.new_move_action(strategy._get_new_hex(), get_entity()))
