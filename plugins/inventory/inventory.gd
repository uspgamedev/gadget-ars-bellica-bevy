extends Component
class_name Inventory

@export var size := 10

var items := {} # slot (int) -> Item

var empty_slots = range(10)

signal updated()

#func _ready():
#	var item = Item.new()
#	item.item_id = "wrench"
#	put_item(item)

func put_item(item: Item) -> bool:
	if empty_slots.is_empty():
		return false
		
	var slot = empty_slots.pop_front()
	items[slot] = item
	
	self.updated.emit()
	return true

func get_item(slot: int) -> Item:
	return items[slot]

func remove_item(slot: int) -> Item:
	if not items.has(slot):
		return null

	var item = items[slot]
	items.erase(slot)
	self.updated.emit()
	
	return item
