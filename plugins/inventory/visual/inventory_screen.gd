extends Control

@export var texture: Texture2D
@onready var grid = self.get_child(0)

func _ready():
	show_inventory.call_deferred()

func _unhandled_input(event: InputEvent):
	if event is InputEventKey and event.is_action_pressed("show_inventory"):
			self.visible = not self.visible

func show_inventory():
	for i in 24:
		var rect = TextureRect.new()
		rect.size_flags_horizontal = SIZE_EXPAND_FILL
		rect.size_flags_vertical = SIZE_EXPAND_FILL
		rect.set_texture(texture)
		grid.add_child(rect)
		
	var entity = Simulation.get_simulation().query([Player, Inventory])[0]
	var inventory := entity.get_component(Inventory) as Inventory
	
	inventory.updated.connect(update_inventory)
	
	update_inventory()

func update_inventory():
	var entity = Simulation.get_simulation().query([Player, Inventory])[0]
	var inventory := entity.get_component(Inventory) as Inventory
	
	for child in grid.get_children():
		child.texture = null
	
	for key in inventory.items.keys():
		var item = inventory.items[key]
		var icon = Item.get_item_dict(item)["icon"]
		grid.get_child(key).set_texture(load(icon))
