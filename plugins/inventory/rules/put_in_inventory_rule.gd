extends Rule

var pickup := PickUpAtrr.new()
var target := TargetAttr.new()

func _check(_simulation: Simulation):
	if not is_instance_valid(target.entity):
		prevent()
		return
		
	var entity = target.entity as Entity
	var item := pickup.item
	
	var item_dict = Item.get_item_dict(item)
		
	if item_dict.has("grabbable") and entity.get_component(Hand) and entity.get_component(Hand).can_put_gadget():
		pass
	elif item_dict.has("modifier") and entity.get_component(Hand) and entity.get_component(Hand).get_gadget():
		pass
	elif entity.get_component(Inventory):
		pass
	else:
		prevent()

	
func _apply(_simulation: Simulation):
	if is_instance_valid(target.entity):
		var entity = target.entity as Entity
		var item := pickup.item
		
		var item_dict = Item.get_item_dict(item)
		
		if item_dict.has("grabbable") and entity.get_component(Hand) and entity.get_component(Hand).can_put_gadget():
			var scene = item_dict["grabbable"]["scene"]
			var gadget = load(scene).instantiate()
			
			for mod_id in item_dict["modifiers"]:
				var mod_item = Item.new(mod_id)
				gadget.add_child(load(Item.get_item_dict(mod_item)["modifier"]["scene"]).instantiate())
			
			
			entity.get_component(Hand).put_gadget(gadget)
			
		elif item_dict.has("modifier") and entity.get_component(Hand).get_gadget():
			var scene = item_dict["modifier"]["scene"]
			entity.get_component(Hand).get_gadget().add_child(load(scene).instantiate())
			
		else:
			var inventory: Inventory = entity.get_component(Inventory)
			
			if inventory:
				inventory.put_item(item)
