extends Camera3D

@onready var player: Entity =  get_tree().get_first_node_in_group("PLAYER")
@onready var player_hex: Hex
@onready var target_position := Vector3()
@onready var holder := self.position

var remote_transform : RemoteTransform3D

func _ready():
	
	if is_instance_valid(player):
		remote_transform = RemoteTransform3D.new()
		player_hex = player.get_component(Hex)
		player_hex.add_child(remote_transform)
		remote_transform.position = position
		remote_transform.rotation = rotation
		remote_transform.update_rotation = false
		remote_transform.update_scale = false
		remote_transform.remote_path = remote_transform.get_path_to(self)
		player_hex.tree_entered.connect(func(): remote_transform.remote_path = remote_transform.get_path_to(self))
		
		global_transform.origin = player_hex.global_position - self.global_position
		

func _process(_delta):
	look_at(get_target_position())

func _input(event):
	#Move camera when user press left button plus move mouse
	if (event is InputEventMouseMotion) && Input.get_mouse_button_mask() & MOUSE_BUTTON_MASK_LEFT:
		var mouseMotion = event as InputEventMouseMotion
		var dir = mouseMotion.relative
		var angle = dir.x * 10e-3 * 0.5
		var playerCamVec: Vector3 = (self.global_position - get_target_position()).rotated(

			Vector3.UP, angle
		)
		if is_instance_valid(remote_transform):
			remote_transform.global_position = get_target_position() + playerCamVec
			
func get_target_position() -> Vector3:
	if is_instance_valid(player):
		target_position = player_hex.global_position
		return player_hex.global_position
	return target_position
	
