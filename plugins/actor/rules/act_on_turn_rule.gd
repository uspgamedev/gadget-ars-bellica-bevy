extends Rule

var turn := TurnAttr.new()

func _apply(simulation: Simulation):
	var entities := simulation.query([Actor, ActOnTurn])
	
	for entity in entities:
		var act_on_turn = entity.get_component(ActOnTurn)
		
		if act_on_turn.advance_turn():
			simulation.apply_action.bind(Action.new_action([ActAttr.new(), TargetAttr.new(entity)] ,"Act")).call_deferred()
	
