extends Rule

var act := ActAttr.new()
var target := TargetAttr.new()

func _check(_simulation: Simulation):
	if target.entity.get_component(Actor) == null:
		prevent()
		

func _apply(_simulation: Simulation):
	target.entity.get_component(Actor).act()
	
