extends Node
class_name ActorStrategy

func get_entity() -> Entity:
	var e = get_parent()
	while e and not e is Entity:
		e = e.get_parent()
	return e

func act() -> bool:
	push_error("Virtual Method")
	return false
