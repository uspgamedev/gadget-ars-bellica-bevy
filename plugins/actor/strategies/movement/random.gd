extends ActorStrategy
class_name RandomStrategy

func act() -> bool:
	var e = get_entity()
	var direction = MapConstants.DIRECTIONS.pick_random() as Vector2i
	return Simulation.apply_action(MapPlugin.new_move_action(direction, e))
