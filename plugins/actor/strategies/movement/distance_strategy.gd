extends ActorStrategy
class_name DistanceStrategy

@export var to_tile: TileRequester
@export var limit_distance: float

func act() -> bool:
	var target = to_tile.request()

	if target == null:
		return get_child(1).act()

	if MapConstants.length(get_entity().get_component(Hex).tile - target) < limit_distance:
		return get_child(0).act()
	else:
		return get_child(1).act()
