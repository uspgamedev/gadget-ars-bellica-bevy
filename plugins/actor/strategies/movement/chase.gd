extends ActorStrategy
class_name ChaseStrategy

@export var chaser_entity: EntityRequester = DefaultEntityRequester.new(self)
@export var target_tile: TileRequester

func act() -> bool:
	var tile = target_tile.request()
	#var position = get_entity().get_component(Hex).tile
			
	if tile:
		var from_tile = chaser_entity.request().get_component(Hex).tile
		var tile_path = Map.get_map().get_tile_path(from_tile, tile)
		
		if not tile_path.is_empty():
			var move_direction = tile_path[1] - from_tile
			return Simulation.apply_action(MapPlugin.new_move_action(move_direction, chaser_entity.request()))
			
		
	return false
