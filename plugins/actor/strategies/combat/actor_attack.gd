extends ActorStrategy
class_name AttackActorStrategy

@export var strategy: AttackStrategy
@export var tile_requester: TileRequester = DefaultTileRequester.new(self)

@export_enum("jump", "slam") var attack_animation := 0

signal attack_begin()

func act() -> bool:
	var tile = tile_requester.request()
	
	attack_begin.emit()
	var attack_result := await strategy.attack(Action.new_action([AttackAttr.new() , HexAttr.new(tile)], ""))
	return attack_result
