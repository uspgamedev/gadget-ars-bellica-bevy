extends ActorStrategy


func act() -> bool:
	for child in get_children():
		if child.act():
			return true
	return false
