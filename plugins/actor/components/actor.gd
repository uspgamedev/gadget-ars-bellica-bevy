extends Component
class_name Actor

var current_strategy: ActorStrategy

func _ready():
	if get_children().size() > 0 and get_child(0) is ActorStrategy:
		current_strategy = get_child(0)

func act():
	await current_strategy.act()
