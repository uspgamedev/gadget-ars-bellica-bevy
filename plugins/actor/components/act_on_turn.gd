extends Component
class_name ActOnTurn

@export var turn_interval := 1

var turn_counter := 0

func advance_turn() -> bool:
	turn_counter += 1
	if turn_counter >= turn_interval:
		turn_counter = 0
		return true
	return false
	
