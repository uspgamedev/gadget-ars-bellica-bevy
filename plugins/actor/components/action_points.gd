extends Component
class_name ActionPoints

@export var max_points := 1
@export var points := 1 : set = set_points

signal empty()

func set_points(ap):
	points = ap
	if ap <= 0:
		empty.emit()
