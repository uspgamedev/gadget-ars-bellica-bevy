extends Rule

var label := AttackAttr.new()
var target := TargetAttr.new()
var amount := AmountAttr.new()


func _modify(_simulation: Simulation):
	# TODO: check rule
	if is_instance_valid(target.entity):	
		amount.value = - amount.value
		add_attribute(HealthAttr.new())
	else:
		prevent()
