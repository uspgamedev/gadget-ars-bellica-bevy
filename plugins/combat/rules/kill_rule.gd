extends Rule

var kill := KillAttr.new()
var target := TargetAttr.new()


func _apply(_simulation: Simulation):
	target.entity.queue_free()
