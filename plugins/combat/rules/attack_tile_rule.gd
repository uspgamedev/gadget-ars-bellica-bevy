extends Rule

var label := AttackAttr.new()
var target := HexAttr.new()
var amount := AmountAttr.new()

func _check(_simulation: Simulation):
	if action.get_attribute(TargetAttr):
		prevent()

var count = false

func _modify(_simulation: Simulation):
	var entities = Map.get_map().get_entities_on_hex(target.value)
	
	for entity in entities:
		var new_action = action.duplicate_action()
		new_action.add_attribute(TargetAttr.new(entity))
		new_action.remove_attribute(HexAttr)
		
		Simulation.apply_action(new_action)
				
	
