extends Rule

var label := HealthAttr.new()
var target := TargetAttr.new()
var amount := AmountAttr.new()


func _apply(_simulation: Simulation):
	var entity := target.entity

	var health = entity.get_component(Health)

	if health:
		health.health += amount.value

		if health.health <= 0:
			Simulation.apply_action(CombatPlugin.new_kill_action(TargetAttr.new(target.entity)))
