extends Rule

var attack := AttackAttr.new()
var team := TeamAttr.new()
var target := TargetAttr.new()


func _check(_simulation: Simulation):
	# TODO: check rule
	if not is_instance_valid(target.entity) or target.entity.is_in_group(team.team):
		prevent()
