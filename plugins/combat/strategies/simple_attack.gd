extends AttackStrategy
class_name SimpleAttackStrategy

@export var damage := 50

@export var from: EntityRequester = DefaultEntityRequester.new(self)

func _init(_damage := 50):
	damage = _damage

func _attack(action: Action) -> bool:
	# TODO: 
	var amount : AmountAttr = action.get_attribute(AmountAttr)
	if action.get_attribute(AmountAttr):
		amount.value += damage
	else:
		action.add_attribute(AmountAttr.new(damage))
	if not is_instance_valid(action.get_attribute(FromAttr)):
		action.add_attribute(FromAttr.new(from.request()))

	return Simulation.apply_action(action)
