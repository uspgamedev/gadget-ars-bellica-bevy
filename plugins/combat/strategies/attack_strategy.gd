extends Node
# TODO: Maybe create a generic super class
class_name AttackStrategy

var decorator := true

func _init(decorator_ := true):
	decorator = decorator_
	
# TODO: Add a preview() -> Array[Action]
# TODO: Apply to all children?
func attack(action: Action) -> bool:
	if decorator and get_child_count() > 0 and get_child(0) is AttackStrategy:
		await get_child(0).attack(action)
	return await _attack(action)

func _attack(_action: Action) -> bool:
	push_error("Interface method")
	return false

func combine_strategy(strategy: AttackStrategy):
	for child in get_children():
		if child is AttackStrategy:
			child.combine_strategy(strategy)
			return
			
	add_child(strategy)
