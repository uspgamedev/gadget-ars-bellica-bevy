extends AttackStrategy
class_name AreaAttackStrategy

@export var outer_radius := 2
@export var inner_radius := 1
@export var include_center := false
@export var damage := 10

@export var color := Color.WHITE

@export var from: EntityRequester = DefaultEntityRequester.new(self)
var animation: AreaAnimation = AreaAnimation.new()

#@onready var visual_map = Map.get_map().visual

signal area_attack(attack: AreaAttackStrategy, target: Vector2i)

func _attack(action: Action) -> bool:
	var tile = action.get_attribute(HexAttr).value
	var tiles = MapConstants.get_hexes_on_area(tile, outer_radius, inner_radius)
	
	area_attack.emit(self, tile)
	
	play_animation(tile)

	var simple = SimpleAttackStrategy.new()
	simple.damage = damage
	simple.from = from
	
	if not action.get_attribute(FromAttr):
		action.add_attribute(FromAttr.new(from.request()))
	action.remove_attribute(TargetAttr)
	
	for target_tile in tiles:
		var new_action = action.duplicate_action()
		new_action.get_attribute(HexAttr).value = target_tile
		
		simple.attack(new_action)
		
	return true

func play_animation(target: Vector2i):
	var new_animation: AreaAnimation = animation.duplicate()
	
	new_animation.inner_radius = inner_radius
	new_animation.outer_radius = outer_radius
	new_animation.color = color
	Simulation.get_simulation().add_child(new_animation)
	new_animation.play(target)

#
## TODO: create an area attack animation node
#var jumping := false
#var tiles3 = []
#
#func jump(target: Vector2i):
	#jumping = true
	#var tiles2 = {}
	#tiles3.push_back(tiles2)
	#var timer
	#
	#for ring_radius in range(inner_radius+1, outer_radius+1):
		#tiles2[ring_radius] = []
		#for hex in MapConstants.get_hexes_on_ring(target, ring_radius):
			#tiles2[ring_radius].push_back(
				#visual_map.add_tile(hex, Color(color, 0.5))
			#)
		#
		#timer = get_tree().create_timer(0.2)
		#timer.timeout.connect((
			#aux
		#).bind(tiles2[ring_radius].duplicate()))
		#
		#await self.create_tween().tween_interval(0.025).finished
	#
	#await timer.timeout
	#
	#for tile in tiles2:
		#AreaAttackStrategy.aux(tile)
	#
	#jumping = false
		#
#static func aux(tiles):
	#var visual_map_ = Map.get_map().visual
	#
	#for tile in tiles:
		#visual_map_.remove_tile(tile)
#
#func _exit_tree():
	#if jumping:
		#for tiles in tiles3:
			#for tile in tiles:
				#AreaAttackStrategy.aux(tiles[tile])
