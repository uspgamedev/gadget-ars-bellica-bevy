extends AttackStrategy
class_name AttributeAttackStrategy

@export var attributes: Array[Attribute] = [] 

func _attack(action: Action) -> bool:
	for attr in attributes:
		action.add_attribute(attr.duplicate())

	return true
