extends AttackStrategy
class_name DamageDecorator

@export var add_damage = 50

func _attack(action: Action) -> bool:
	var amount = action.get_attribute(AmountAttr)
	if amount:
		amount.value += add_damage
	else:
		action.add_attribute(AmountAttr.new(add_damage))
	return true	
