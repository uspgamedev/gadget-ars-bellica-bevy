extends AttackStrategy
class_name TeamAttackStrategy

@export var team := ""

func _attack(action: Action) -> bool:
	action.add_attribute(TeamAttr.new(team))
	return true
