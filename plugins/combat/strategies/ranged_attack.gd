extends AttackStrategy

## Tile/turn
@export var speed := 5.0
@export var max_range := 3
@export var items_to_drop : Array[ItemRequester] = []
@export var remove_gadget := true

@export var remove := true

@export var projectile_scene: PackedScene
func _init():
	super._init(false)

func _attack(action: Action) -> bool:
	var tile = action.get_attribute(HexAttr).value 
	var from_entity = action.get_attribute(FromAttr).entity
	var from = from_entity.get_component(Hex).tile
	var projectile = projectile_scene.instantiate()
	
	if remove:
		from_entity.get_component(Hand).remove_gadget()
	
	Simulation.get_simulation().add_child(projectile)
	
	action.remove_attribute(HexAttr)
	
	projectile.get_component(Projectile).attack_strategy = get_projectile_strategy()
	projectile.get_component(DropItem).items.append_array(items_to_drop.map(func(x: ItemRequester): return x.request()))
	await projectile.get_component(Projectile).shoot(from, tile - from, max_range, action)

	# TODO: Add ShootAttr
	return true

func get_projectile_strategy() -> AttackStrategy:
	if get_child(0):
		return get_child(0) as AttackStrategy
	return SimpleAttackStrategy.new()

