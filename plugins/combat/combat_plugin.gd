extends RefCounted
class_name CombatPlugin


static func new_attack_action(target: TargetAttr, damage: AmountAttr) -> Action:
	return Action.new_action([AttackAttr.new(), target, damage], "Attack")


static func new_change_health_action(target: TargetAttr, damage: AmountAttr) -> Action:
	return Action.new_action([HealthAttr.new(), target, damage], "ChangeHealth")


static func new_kill_action(target: TargetAttr) -> Action:
	return Action.new_action([KillAttr.new(), target], "Kill")
