extends Component3D
class_name Health

@export var max_health := 100:
	set = set_max_health
@export var health := 100:
	set = set_health

const DAMAGE_DELAY := 0.5


func _ready():
	$HealthLabel.text = str(health) + "/" + str(max_health)


func set_health(new_health):
	if get_entity():
		var body := get_entity().get_component(Body) as Body
		if body and new_health > 0.0:
			var color := Color.WHITE
			if health - new_health < 0.0:
				color = Color.GREEN
			if health - new_health > 0.0:
				color = Color.RED
			body.set_modulate(color, true)

	health = new_health
	update_health_label()


func set_max_health(new_max_health):
	max_health = new_max_health
	update_health_label()


func update_health_label():
	$HealthLabel.text = str(health) + "/" + str(max_health)
#func set_health(amount: int):
#	if amount <= 0:
#		action.target.entities = [self.get_entity()] as Array[Entity]
#		Simulation.apply_action(action)
#	return amount
