extends BehaviorNode
class_name ChoiceDistance

@export var target_type: SearchStrategy
@export var limit_distance: float

func distance_to_target(entity: Entity, target: Entity) -> float:
	var pos = entity.get_component(Hex).tile
	var target_pos = target.get_component(Hex).tile
	return (target_pos - pos).length()

func act() -> bool:
	var e = EntityUtil.get_entity(self)
	var targets = Simulation.get_simulation().query([target_type])

	if targets.is_empty():
		return get_child(1).act()

	for t in targets:
		if distance_to_target(e, t) < limit_distance:
			return get_child(0).act()
		else:
			return get_child(1).act()
	return false

