extends TileRequester
class_name AimToTile

@export var entity: EntityRequester = DefaultEntityRequester.new(self)

func request() -> Vector2i:
	return entity.request().get_component(Aim).tile + entity.request().get_component(Hex).tile
