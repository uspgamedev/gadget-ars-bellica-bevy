extends TileRequester
class_name DefaultTileRequester

var node: Node

func request():
	return EntityUtil.get_entity(node).get_component(Hex).tile

func _init(node_: Node):
	node = node_
