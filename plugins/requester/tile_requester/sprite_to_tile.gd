@tool
extends TileRequester
class_name SpriteToTile

@export var tile := Vector2i() : set = set_tile

func request() -> Vector2i:
	return tile

func _ready():
	if not Engine.is_editor_hint():
		self.visible = false

func _process(_delta):
	if Engine.is_editor_hint():
		tile = MapConstants.position_2d_to_hex(self.global_position)
		self.global_position = MapConstants.hex_to_position_2d(tile)

func set_tile(new_tile: Vector2i):
	tile = new_tile
	self.global_position = MapConstants.hex_to_position_2d(tile)
	
