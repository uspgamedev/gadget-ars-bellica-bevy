extends EntityRequester
class_name ComponentsToEntity

@export var component_types: Array[Script]

# Todo: Create a CloserEntityRequest
func request():
	var entities = Simulation.get_simulation().query(component_types)
	
	if entities.size() > 1:
		push_warning("Multiple entities")
		
	if entities.size() > 0:
		return entities[0]
		
	return null
		

func request_multiple() -> Array:
	return Simulation.get_simulation().query(component_types)
