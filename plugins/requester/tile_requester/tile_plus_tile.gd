extends TileRequester
class_name TilePlusTile

@export var tile_1 :TileRequester = DefaultTileRequester.new(self)
@export var tile_2 :TileRequester = DefaultTileRequester.new(self) 

func request() -> Vector2i:
	return tile_1.request() + tile_2.request()
