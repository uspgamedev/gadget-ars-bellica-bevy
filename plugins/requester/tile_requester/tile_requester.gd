extends Node
class_name TileRequester

func request():
	push_error("Virtual method")
	return null

func request_multiple() -> Array[Vector2i]:
	if request() != null:
		return [request()]
	return []
