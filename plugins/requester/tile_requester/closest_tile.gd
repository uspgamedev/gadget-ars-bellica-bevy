extends TileRequester
class_name ClosestTile

@export var from_tile: TileRequester
@export var to_tiles: TileRequester

func request():
	var position = from_tile.request()
	var tiles = to_tiles.request_multiple()
	
	if tiles.is_empty():
		return null
			
	var closest_target_pos = null
	var closest_length := INF
	
	for tile in tiles:
		if MapConstants.length(tile - position) < closest_length:
			closest_target_pos = tile
			closest_length = MapConstants.length(tile - position)
	
	return closest_target_pos
