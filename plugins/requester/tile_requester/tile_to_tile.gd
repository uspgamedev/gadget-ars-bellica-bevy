extends TileRequester
class_name TileToTile

@export var tile := Vector2i()

func request() -> Vector2i:
	return tile
