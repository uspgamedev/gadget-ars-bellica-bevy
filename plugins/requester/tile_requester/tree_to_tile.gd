extends TileRequester
class_name TreeToTile

@export var root: Node

func request_multiple():
	var tiles = []
	
	for child in root.get_children():
		if child is Entity and child.get_component(Hex):
			tiles.append(child.get_component(Hex).tile)	
	
	return tiles
			
