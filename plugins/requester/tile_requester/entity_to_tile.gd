extends TileRequester
class_name EntityToTile

@export var entity: Entity
@export var entity_requester: EntityRequester

func request():
	var current_entity = entity
	if entity_requester:
		current_entity = entity_requester.request()
	
	if not is_instance_valid(current_entity) or current_entity.get_component(Hex) == null:
		push_error("Entity not valid")
		return null
	return current_entity.get_component(Hex).tile

func request_multiple() -> Array[Vector2i]:
	var tiles: Array[Vector2i] = []
	var entities = []
	if entity_requester:
		entities = entity_requester.request_multiple()
	else:
		if not is_instance_valid(entity) or entity.get_component(Hex) == null:
			return []
		entities.push_back(entity)

	for e in entities:
		var hex = e.get_component(Hex)
		if hex:
			tiles.push_back(hex.tile)

	return tiles
		
		
