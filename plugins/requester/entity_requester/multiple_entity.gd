extends EntityRequester
class_name MultipleEntity

@export var entities: Array[EntityRequester]

func request_multiple() -> Array[Entity]:
	var entities_: Array[Entity] = []
#	var real_node_path = str(node_path).right(-3)
	
	for entity_array in entities:
		entities_.append_array(entity_array.request_multiple())
	
	return entities_
			
