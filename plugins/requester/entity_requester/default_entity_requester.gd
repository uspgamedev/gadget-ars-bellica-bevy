extends EntityRequester
class_name DefaultEntityRequester

var node: Node

func request():
	return EntityUtil.get_entity(node)

func _init(node_: Node):
	node = node_
