extends EntityRequester
class_name TileToEntity

@export var tile: TileRequester = DefaultTileRequester.new(self)

func request() -> Entity:
	var entities = request_multiple()
	if entities.is_empty():
		return null
		
	return entities[0]

func request_multiple() -> Array[Entity]:
	var entities: Array[Entity] = []
	for tile_ in tile.request_multiple():
		var entity = Map.get_map().get_entities_on_hex(tile_)
		entities.append_array(entity)
		
	return entities
			
