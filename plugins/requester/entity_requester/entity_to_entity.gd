extends EntityRequester
class_name EntityToEntity

@export var entity: Entity
@export var entities: Array[Entity]


func request() -> Entity:
	if is_instance_valid(entity):
		return entity as Entity
	return null

func request_multiple() -> Array[Entity]:
	var _entities: Array[Entity] = []
	if is_instance_valid(entity):
		_entities.append(entity)
	
	for _entity in entities:
		if is_instance_valid(_entity):
			_entities.append(_entity)
	return _entities
