extends EntityRequester
class_name TreeToEntity

@export var node: Node

func request_multiple() -> Array[Entity]:
	var entities_: Array[Entity] = []
	
	for child in node.get_children():
		if child is Entity:
			entities_.append(child)	
	
	return entities_
			
