extends EntityRequester
class_name GroupToEntity

@export var group: String

# Todo: Create a CloserEntityRequest
func request():
	var entities = get_tree().get_nodes_in_group(group)
	
	if entities.size() > 1:
		push_warning("Multiple entities")
		
	if entities.size() > 0:
		return entities[0]
		
	return null
		

func request_multiple() -> Array:
	return get_tree().get_nodes_in_group(group)
