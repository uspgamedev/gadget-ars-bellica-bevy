extends Node
class_name EntityRequester



func request() -> Entity:
	push_error("Virtual method")
	return null
	
func request_multiple() -> Array[Entity]:
	if is_instance_valid(request()):
		return [request()]
	return []
