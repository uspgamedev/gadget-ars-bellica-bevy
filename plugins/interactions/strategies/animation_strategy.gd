extends InteractionStrategy

@export var animation : Array[String] = []
@export var entity: EntityRequester = DefaultEntityRequester.new(self)

var count := 0

func interact(_action: Action):
	if animation.size() > 0:
		entity.request().get_component(Body).set_animation(animation[count])
		count += 1
		count %= animation.size()
	
