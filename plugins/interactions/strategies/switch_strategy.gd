@tool
extends InteractionStrategy

@export var target_tile : Vector2i
@export var target_spawner : NodePath

func _process(_delta):
	if Engine.is_editor_hint() and target_spawner:
		target_tile = get_node(target_spawner).tile

func interact(_action: Action):
	var map := Map.get_map() 
	var targets := SingleSearchStrategy.new().search(map, target_tile)
	if targets.size() > 0:
		Simulation.apply_action(
			InteractionPlugin.new_interact_action(TargetAttr.new(targets.front()))
			.add_attribute(SwitchAttr.new())
		)
	
