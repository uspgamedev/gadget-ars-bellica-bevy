extends InteractionStrategy

@export_multiline var description := ""

func interact(_action: Action):
	print(description)
