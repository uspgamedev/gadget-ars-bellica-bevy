extends InteractionStrategy
class_name SpawnStrategy

@export var tile_to_spawn: TileRequester = DefaultTileRequester.new(self)
@export var entity_scene: PackedScene
@export var to_spawn_parent: Node 

func interact(_action: Action):
	var tiles = tile_to_spawn.request_multiple()
	
	for tile in tiles:
		var entity = entity_scene.instantiate()
		if entity.has_node("Hex"):
			entity.get_node("Hex").tile = tile
		
		# TODO: remove this
#		await get_tree().physics_frame
		# TODO: Create Attribute and Rule for spawning
		if to_spawn_parent:
			to_spawn_parent.add_child.bind(entity).call_deferred()
		else:
			Simulation.get_simulation().add_child.bind(entity).call_deferred()
	

# TODO: Create SpawnRequest: Spawn Tile, in Group, in tree
