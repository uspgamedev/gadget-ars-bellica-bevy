extends InteractionStrategy
class_name IfInteractStrategy

@export var conditions : Array[Script] = []
@export var negative_conditions : Array[Script] = []

@export var else_node: Node

func interact(action: Action):
	var match_condition := true

	for condition in conditions:
		if action.get_attribute(condition) == null:
			match_condition = false
	for condition in negative_conditions:
		if action.get_attribute(condition) != null:
			match_condition = false

	if match_condition:
		for child in get_children():
			if child is InteractionStrategy:
				child.interact(action)
	elif else_node:
		for child in else_node.get_children():
			if child is InteractionStrategy:
				child.interact(action)

