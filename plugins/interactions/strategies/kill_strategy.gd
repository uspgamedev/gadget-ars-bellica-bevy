extends InteractionStrategy
class_name KillStrategy

@export var to_kill: EntityRequester = DefaultEntityRequester.new(self)

func interact(_action: Action):
	var entities = to_kill.request_multiple()
	
	for entity in entities:
		if is_instance_valid(entity):
			if entity.get_parent():
				entity.get_parent().remove_child(entity)			
			entity.queue_free()
	
