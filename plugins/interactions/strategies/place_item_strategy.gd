extends InteractionStrategy

@export var item_ids: Array[String] = []

signal placed_all_items()

func interact(_action: Action):
	# TODO: create a rule
	var player = Simulation.get_simulation().query([Player])[0]
	var inventory: Inventory = player.get_component(Inventory) 
	
	for slot in inventory.items.keys():
		var item = inventory.items[slot]
		if item_ids.find(item.item_id) != -1:
			item_ids.pop_at(item_ids.find(item.item_id))
			
			inventory.remove_item(slot)
			
			if item_ids.is_empty():
				placed_all_items.emit()
	
