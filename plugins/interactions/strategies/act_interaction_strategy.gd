extends InteractionStrategy

@export var entity_requester: EntityRequester = DefaultEntityRequester.new(self)

func interact(_action: Action):
	var entity: Entity
	if entity_requester:
		entity = entity_requester.request()
	else:
		entity = EntityUtil.get_entity(self)
		
	entity.get_component(Actor).act()
