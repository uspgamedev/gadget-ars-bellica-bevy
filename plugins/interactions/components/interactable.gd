extends Component
class_name Interactable

# TODO: merge with actor and remove InteractAttr

func interact(action: Action):
	var strategies = get_children()
	strategies.append_array(get_entity().get_children())
	for strategy in strategies:
		if strategy is InteractionStrategy:	
			strategy.interact(action)
