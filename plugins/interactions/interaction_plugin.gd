extends Node
class_name InteractionPlugin


static func new_interact_action(target: TargetAttr) -> Action:
	return Action.new_action([InteractAttr.new(), target], "Interact")
