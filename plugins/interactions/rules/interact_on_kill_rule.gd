extends Rule

var kill = KillAttr.new()
var target = TargetAttr.new()
	
func _modify(_simulation: Simulation):
	if target.entity.get_component(InteractOnKill) and target.entity.get_component(Interactable):
		add_attribute(InteractAttr.new())

