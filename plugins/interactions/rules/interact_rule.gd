extends Rule

var label := InteractAttr.new()
var target := TargetAttr.new()

func _check(_simulation: Simulation):
	if target.entity == null:
		prevent()
		return
		
	if target.entity.get_component(Interactable) == null:
		prevent()
	
func _apply(_simulation: Simulation):
	var interactable := target.entity.get_component(Interactable) as Interactable

	var new_action = self.action.duplicate_action()
	interactable.interact(new_action)
	

	
