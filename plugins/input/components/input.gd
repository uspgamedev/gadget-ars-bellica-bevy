extends Component

@export var center_tile: TileRequester
@export var color := Color.GREEN

@export var tile_inputs: Array[TileRequester] = []

var current_tile := Vector2i()
var tile_id = -1

var mode := 0
var input_type := 0

var strategies: Array[InputStrategy] = []
var current_strategy := 0

func _ready():
	if strategies.is_empty():
		for strategy in get_children():
			if strategy is InputStrategy:
				strategies.push_back(strategy)
				remove_child.bind(strategy).call_deferred()
				
		if strategies.size() > 0:
			add_child.bind(strategies[0]).call_deferred()

func _process(_delta):
	strategies[current_strategy]._preview(get_target_tile())

func _unhandled_input(event):
	if event.is_action_released("change_input_mode"):
		mode = (mode + 1)%tile_inputs.size()
		return
		
	if event.is_action_released("change_input_type"):
		current_strategy = (current_strategy + 1) % strategies.size()
		for strategy in get_children():
			if strategy is InputStrategy:
				remove_child(strategy)
		if strategies.size() > 0:
			add_child(strategies[current_strategy])
		
	for strategy in get_children():
		if strategy is InputStrategy and strategy._can_apply(event):
			strategy._apply(get_target_tile())
#
#func update_visual_input(tile: Vector2i):
	#if current_tile != tile:
		#var visual_map := Map.get_map().visual
		#if tile_id != -1:
			#visual_map.remove_tile(tile_id)
		#tile_id = visual_map.add_tile(tile, color)
		#current_tile = tile

func get_target_tile() -> Vector2i:
	return tile_inputs[mode].request() + center_tile.request()
