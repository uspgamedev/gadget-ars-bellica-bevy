extends Node
class_name InputStrategy

func _can_apply(_event: InputEvent) -> bool:
	push_error("Virtual Method")
	return false

func _apply(_target: Vector2i):
	push_error("Virtual Method")
	pass

func _preview(_target: Vector2i):
	#push_error("Virtual Method")
	pass
