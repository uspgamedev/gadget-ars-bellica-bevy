extends TileRequester

var target = Vector2()
var tile_range := 100

func request() -> Vector2i:
	return target

func _input(event):
	if event is InputEventMouseMotion:
		target = mouse_to_hex(event.position) - mouse_to_hex(MapConstants.CENTER_POSITION)
		if MapConstants.length(target) > tile_range:
			var dis = null
			var current_tile
			
			for tile in MapConstants.get_hexes_on_ring(Vector2i(), tile_range+1):
				var new_dis = mouse_to_position(event.position) - mouse_to_position(MapConstants.CENTER_POSITION) \
						- MapConstants.hex_to_position(tile) 
				new_dis = new_dis.length()
				
				if dis == null or new_dis < dis:
					current_tile = tile
					dis = new_dis
			
			target = current_tile

func mouse_to_hex(mouse_pos: Vector2) -> Vector2i:
	var pos = mouse_to_position(mouse_pos)
	return MapConstants.position_to_hex(pos)

func mouse_to_position(mouse_pos: Vector2) -> Vector3:
	var camera := get_viewport().get_camera_3d()
	
	var origin = camera.project_ray_origin(mouse_pos)
	var normal = camera.project_ray_normal(mouse_pos)
	
	return origin - origin.y * normal / normal.y
