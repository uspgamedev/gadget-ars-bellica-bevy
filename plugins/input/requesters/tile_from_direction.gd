extends TileRequester

var desire_displacement := Vector2i(2, 0)
var real_displacement = Vector2()

const FADE = 0.2
const WEIGHT = 0.4

var tile_id = -1

func request() -> Vector2i:
	return desire_displacement

func _process(_delta):
		# TODO: Bug solved in 4.2 https://github.com/godotengine/godot/issues/45628
#		var real_displacement := Input.get_vector("move_left", "move_right", "move_up", "move_down")	
		
		var camera_angle = wrapf(get_viewport().get_camera_3d().rotation.y, 0, 2*PI) if get_viewport().get_camera_3d() else 0.0
		camera_angle = PI/3 * round(camera_angle/(PI/3))
			
		if Input.is_action_pressed("move_left"):
			real_displacement.x -= 0.5
		if Input.is_action_pressed("move_right"):
			real_displacement.x += 0.5
		if Input.is_action_pressed("move_down"):
			real_displacement.y += 0.5
		if Input.is_action_pressed("move_up"):
			real_displacement.y -= 0.5
		
		
		if real_displacement.length_squared() < 1.0:
			return
			
		real_displacement /= 1.0 + FADE
		
		real_displacement = real_displacement.rotated(-camera_angle)
			
		var displacement : Vector2i 
		
		var distance = 10000.0
		for direction in MapConstants.DIRECTIONS:
			var aux = (real_displacement - (direction as Vector2).normalized())
			if aux.length_squared() < distance:
				distance = aux.length_squared()
				displacement = direction

		real_displacement = real_displacement.rotated(camera_angle)
		
		if MapConstants.DIRECTIONS.find(displacement) != -1 and desire_displacement != displacement:
			desire_displacement = displacement
			#update_visual_input()
