extends Component
class_name Blocking

enum Level {
	Ground = 0,
	Air = 1,
	Hole = 2,
	Bridge = 3,
}

@export_flags("Ground:1", "Air:2", "Hole:4", "Bridge:8") var levels := 1 

func _init(levels_ := 1):
	levels = levels_


func get_levels() -> Array:
	var result = []
	for level in Level.values():
		var value = 2**level
		if value & levels:
			result.append(level)
	
	return result

static func filter_by_level(entity, on_levels) -> bool:
	var blocking = entity.get_component(Blocking)
	if blocking == null:
		return false
	
	for level in on_levels:
		if blocking.get_levels().has(level):
			return true
			
	return false
