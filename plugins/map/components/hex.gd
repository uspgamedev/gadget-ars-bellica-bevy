@tool
extends Component3D
class_name Hex

const MOVEMENT_DELAY := 0.5

signal start_moving(direction: Vector2i)
signal stop_moving()


var tile: Vector2i = Vector2i() : set = set_tile
var moving_tween : Tween = null

signal hex_changed(new_tile: Vector2i)

var update_position := true

func set_tile(new_tile: Vector2i):
	if tile != new_tile:
		hex_changed.emit(new_tile)
		
	if update_position:
		if is_inside_tree():
			global_position = MapConstants.hex_to_position(new_tile)
		else:
			position = MapConstants.hex_to_position(new_tile)
	tile = new_tile

func _ready():
	if Engine.is_editor_hint():
		return
	
	update_tile()
	
	Simulation.apply_action(MapPlugin.new_register_action(get_entity(), tile))
		
func update_tile():	
	tile = MapConstants.position_to_hex(self.global_position)

func move_to(to_tile: Vector2i):
	var direction = to_tile - tile
	
	update_position = false
	tile = to_tile
	update_position = true
	var pos = MapConstants.hex_to_position(tile)
	
	start_moving.emit(direction)
	
	if moving_tween:
		moving_tween.kill() 
	
	moving_tween = create_tween()
	moving_tween.set_ease(Tween.EASE_OUT)
	moving_tween.set_trans(Tween.TRANS_QUAD)
	moving_tween.tween_property(self, "global_position", pos, MOVEMENT_DELAY)
	moving_tween.finished.connect( func():
		moving_tween = null
		stop_moving.emit()
	)

func _exit_tree():
	if Engine.is_editor_hint():
		return
		
	Simulation.apply_action(MapPlugin.new_unregister_action(get_entity()))
