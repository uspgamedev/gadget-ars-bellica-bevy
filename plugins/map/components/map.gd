@tool
extends Component2D
class_name Map

var map: HexMap = HexMap.new()

@export var visual: VisualMap

static func get_map() -> Map:
	return Simulation.get_simulation().query([Map])[0].get_component(Map)

func _ready():
	init_path_finder()
	queue_redraw.call_deferred()

func get_entities_on_hex(hex: Vector2i) -> Array:
	var entities = []
	entities.append_array(map.get_entities_on_hex(hex))
	
	return entities
	
func get_entities_on_hexes(hexes: Array[Vector2i]) -> Array:
	var entities := []
	entities.append_array(map.get_entities_on_hexes(hexes))
	
	return entities

func put_entity(entity: Entity, hex: Vector2i):
	map.put_entity(entity, hex)
	queue_redraw()	
	update_tile(hex)


func remove_entity(entity: Entity, hex: Vector2i):
	map.remove_entity(entity, hex)
	queue_redraw()
	update_tile(hex)


## Respects order
func get_entities_on_line(from: Vector2i, direction: Vector2i, distance: int) -> Array:
	return map.get_entities_on_line(from, direction, distance)

# TODO: Move to VisualMap
func _draw():
	var radius = Vector2(0, -1) * MapConstants.L

	var entities = map.entities
	for hex in entities.keys():
		var point2 = MapConstants.hex_to_position(hex) - %MapSprite.position
		var point = Vector2(point2.x, point2.z)
		var points = []

		for k in range(6):
			points.append(radius.rotated(PI / 3 * k) + point)

		self.draw_colored_polygon(points, Color(125, 0, 0, 0.5))

var path_finder := AStar2D.new() 

const SIZE = 100

func init_path_finder():
	path_finder.reserve_space(5*SIZE**2)

	for i in range(-round(SIZE/2.), round(SIZE/2.)):
		for j in range(-round(SIZE/2.), round(SIZE/2.)):
			var y = i
			var x = 2*j + 1 if (i+SIZE)%2 == 1 else 2*j
			var hex := Vector2i(x, y)
			
			path_finder.add_point(hex_to_id(hex), hex)
			
			
	for i in range(-round(SIZE/2.), round(SIZE/2.)):
		for j in range(-round(SIZE/2.), round(SIZE/2.)):
			var y = i
			var x = 2*j + 1 if (i+SIZE)%2 == 1 else 2*j
			var hex := Vector2i(x, y)
			
			var adjacents = MapConstants.DIRECTIONS.map(func(d): return hex + d)

			for adjacent in adjacents:			
				if path_finder.has_point(hex_to_id(adjacent)):
					path_finder.connect_points(hex_to_id(hex), hex_to_id(adjacent))
	
func hex_to_id(hex: Vector2i) -> int:
	return (hex.x + SIZE) + (hex.y + SIZE) * SIZE
	

func update_tile(tile: Vector2i):
	path_finder.set_point_disabled(hex_to_id(tile), is_tile_blocked(tile))

func is_tile_blocked(tile: Vector2i, levels = [Blocking.Level.Ground]) -> bool:
	#var levels = Map.bitfield_to_levels(blocking.levels)
	
	#if levels.has(Map.Level.Ground):# and not levels.has(Map.Level.Underground):
		##levels.push_back(Map.Level.Underground)
		#map.get_entities_on_hex(new_hex, [Map.Level.Underground])
		
	var entities = map.get_entities_on_hex(tile)
	
	var has_hole = false
	var has_bridge = false
	
	for entity in entities:
		var other_blocking = entity.get_component(Blocking)
		if other_blocking:
			if levels.has(Blocking.Level.Ground) and other_blocking.get_levels().has(Blocking.Level.Ground):
				return true
			if levels.has(Blocking.Level.Ground) and other_blocking.get_levels().has(Blocking.Level.Hole):
				has_hole = true
			if levels.has(Blocking.Level.Ground) and other_blocking.get_levels().has(Blocking.Level.Bridge):
				has_bridge = true
	
	if has_hole and not has_bridge:
		return true

	return false
	
	
	
func get_tile_path(from_tile: Vector2i, to_tile: Vector2i) -> Array:
	var disabled = path_finder.is_point_disabled(hex_to_id(from_tile))
	path_finder.set_point_disabled(hex_to_id(from_tile), false)
	var disabled2 = path_finder.is_point_disabled(hex_to_id(to_tile))
	path_finder.set_point_disabled(hex_to_id(to_tile), false)
	
	var tile_path = path_finder.get_point_path(hex_to_id(from_tile), hex_to_id(to_tile))
	
	path_finder.set_point_disabled(hex_to_id(from_tile), disabled)
	path_finder.set_point_disabled(hex_to_id(to_tile), disabled2)
	return Array(tile_path).map(func(t): return Vector2i(t))
	
	
	
	
