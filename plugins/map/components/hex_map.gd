@tool
extends Node2D
class_name HexMap

@export var visual: VisualMap

var entities: Dictionary  # Hex -> Array[Entity]

func _ready():
	queue_redraw.call_deferred()

func get_entities_on_hex(hex: Vector2i) -> Array:
	var e = entities.get(hex)
	return e if e != null else []
	
func get_entities_on_hexes(hexes: Array[Vector2i]) -> Array:
	var result := []
	for hex in hexes:
		result.append_array(get_entities_on_hex(hex))
	return result

func put_entity(e: Entity, hex: Vector2i):
	queue_redraw()
	
	if entities.get(hex):
		entities[hex].push_back(e)
	else:
		entities[hex] = [e]


func remove_entity(e: Entity, hex: Vector2i):
	if not self.get_entities_on_hex(hex).is_empty():
		entities[hex].erase(e)
		if entities[hex].is_empty():
			entities.erase(hex)
		queue_redraw()


func get_entities_on_line(from: Vector2i, direction: Vector2i, distance: int) -> Array:
	var current_tile := from + direction 
	var entity_array = []
	
	for _i in range(distance):
		#if not get_entities_on_hex(current_tile).is_empty():
		entity_array.append_array(get_entities_on_hex(current_tile))
		current_tile += direction
	
	return entity_array
