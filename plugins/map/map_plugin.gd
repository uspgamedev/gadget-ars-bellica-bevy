extends Node
class_name MapPlugin

static func new_register_action(entity, _hex: Vector2i) -> Action:
	var hex := HexAttr.new()
	hex.value = _hex

	var target := TargetAttr.new(entity)

	return Action.new_action([RegisterAttr.new(), hex, target], "RegisterHex")


static func new_unregister_action(entity) -> Action:
	var target := TargetAttr.new()
	target.entity = entity

	return Action.new_action([UnregisterAttr.new(), target], "UnregisterHex")


static func new_move_action(displacement: Vector2i, target) -> Action:
	var amount := AmountAttr.new()
	var direction := DirectionAttr.new()
	var _target := TargetAttr.new()

	amount.value = displacement.length()

	if is_equal_approx(amount.value, 0.0):
		direction.value = Vector2i()
	else:
		direction.value = (displacement as Vector2).normalized()

	_target.entity = target

	return Action.new_action([MovementAttr.new(), _target, amount, direction], "MoveAction")
