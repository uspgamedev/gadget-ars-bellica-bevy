extends Rule

var hex := HexAttr.new()
var target := TargetAttr.new()
var register := RegisterAttr.new()

func _apply(simulation: Simulation):
	var entity := target.entity
	var map_entity := simulation.query([Map])[0] as Entity
	var map := map_entity.get_component(Map) as Map

	map.put_entity(entity, hex.value)
