extends Rule

var target := TargetAttr.new()
var register := UnregisterAttr.new()


func _apply(simulation: Simulation):
	var entity := target.entity
	var map_entity := simulation.query([Map])[0] as Entity
	var map := map_entity.get_component(Map) as Map
	map.remove_entity(entity, entity.get_component(Hex).tile)
	pass
