extends Rule

var label := MovementAttr.new()
var direction := DirectionAttr.new()
var amount := AmountAttr.new()
var target := TargetAttr.new()

func _check(simulation: Simulation):
	if target.entity == null:
		prevent()
		return

	var entity := target.entity

	entity.get_component(Hex)
	var hex := entity.get_component(Hex) as Hex
	if hex == null:
		prevent()
		return

	var maps = simulation.query([Map])
	var map_ent := maps.front() as Entity

	var map := map_ent.get_component(Map) as Map

	var to_tile = hex.tile + (round(amount.value * direction.value) as Vector2i)

	#print(map.get_first_blocking_entity_on_line(hex.tile, to_tile, MapConstants.length(to_tile - hex.tile)))

	if not can_move(map, entity, to_tile): # or \
			#map.get_first_blocking_entity_on_line(hex.tile + MapConstants.get_direction(to_tile), to_tile, MapConstants.length(to_tile - hex.tile)):
		prevent()


func _apply(simulation: Simulation):
	var entity := target.entity
	var hex := entity.get_component(Hex) as Hex

	var map_ent := simulation.query([Map]).front() as Entity
	var map := map_ent.get_component(Map) as Map

	var to_tile = hex.tile + (round(amount.value * direction.value) as Vector2i)
		
	if try_move(map, entity, to_tile):
		hex.move_to(to_tile)
	else:
		push_error("Can't Move")

func can_move(map: Map, entity: Entity, new_hex: Vector2i) -> bool:
	var blocking = entity.get_component(Blocking)
	if blocking == null:
		return true

	#var levels = Map.bitfield_to_levels(blocking.levels)
	
	#if levels.has(Map.Level.Ground):# and not levels.has(Map.Level.Underground):
		##levels.push_back(Map.Level.Underground)
		#map.get_entities_on_hex(new_hex, [Map.Level.Underground])
		
	var entities = map.get_entities_on_hex(new_hex)
	
	var has_hole = false
	var has_bridge = false
	
	for other_entity in entities:
		var other_blocking = other_entity.get_component(Blocking)
		if other_blocking:
			if blocking.get_levels().has(Blocking.Level.Ground) and other_blocking.get_levels().has(Blocking.Level.Ground):
				return false
			if blocking.get_levels().has(Blocking.Level.Ground) and other_blocking.get_levels().has(Blocking.Level.Hole):
				has_hole = true
			if blocking.get_levels().has(Blocking.Level.Ground) and other_blocking.get_levels().has(Blocking.Level.Bridge):
				has_bridge = true
	
	if has_hole and not has_bridge:
		return false

	return true


func try_move(map: Map, entity: Entity, new_hex: Vector2i) -> bool:
	#var blocking = entity.get_component(Blocking)
	#if blocking == null:
		##levels.append_array(Map.bitfield_to_levels(blocking.levels))
		#pass
		
	if can_move(map, entity, new_hex):
		map.remove_entity(entity, entity.get_component(Hex).tile)
		map.put_entity(entity, new_hex)
		return true

	return false
