@tool
extends Sprite2D
class_name ShowEntity2D

@onready var spawner : Spawner = get_parent()

func _ready():
	if spawner.get_child(0) and spawner.get_child(0).get_node("Body"):
		self.texture = spawner.get_child(0).get_node("Body").icon
				
	if !Engine.is_editor_hint():
		queue_free()
		
var pos = null

func _process(_delta):
	
	if pos and pos != global_position:
		spawner.tile = Vector2(global_position.x/8., (global_position.y + 4.)/12.)
	
	self.global_position = MapConstants.hex_to_position_2d(get_parent().tile)
	
	pos = global_position
	
	
	
