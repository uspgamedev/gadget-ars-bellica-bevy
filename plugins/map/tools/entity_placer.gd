@tool
extends TileMap

@export var generate_entities := false : set = set1
@export var total_entities := 0 : set = set2, get = get2
@export var id_to_prototype := {}

@export var entity_scenes : Array[PackedScene] = []

@export var spawner_scene : PackedScene
@export var show_2d_scene : PackedScene

func set1(generate: bool):
	if Engine.is_editor_hint() and generate:
		update_spawners()
		
func set2(_x: int):
	total_entities = get2()
	
	
func get2() -> int:
	return get_used_cells(0).size()
	

#func _enter_tree():
#	if !Engine.is_editor_hint():
#		queue_free()

func _ready():
	if Engine.is_editor_hint():
		update_tile_set.call_deferred()
	
#	if !Engine.is_editor_hint():
#		queue_free()
	
	
#	if Engine.is_editor_hint() and not(get_parent() is SubViewport): # If isn't the scene's root
#		for child in get_children():
#			child.queue_free()


func update_tile_set():
	tile_set = TileSet.new()
	tile_set.tile_shape = TileSet.TILE_SHAPE_HEXAGON
	for i in len(entity_scenes):
		var scene = entity_scenes[i] 
		var texture = entity_scenes[i].instantiate().get_node("Body").icon as Texture2D
		
		texture.get_size()
		
		var atlas := TileSetAtlasSource.new()
		atlas.texture = texture
		atlas.texture_region_size = texture.get_size()
		atlas.use_texture_padding = false
		atlas.create_tile(Vector2i(0, 0))
		tile_set.add_source(atlas, i)
		
		if !id_to_prototype.get(i):
			id_to_prototype[i] = scene
			
func update_tiles():
	for cell in get_used_cells(0):
		set_cell(0, cell)
	
	if not is_instance_valid(get_tree()):
		return
		
	for spawner in get_tree().get_nodes_in_group("SPAWNER"):
		if spawner.prototype_id >= 0:
			var tile = Vector2i(spawner.tile.x/2, spawner.tile.y)
			if spawner.tile.y%2 == 1:
				tile.x += 1
				
			if get_cell_source_id(0, tile) != spawner.prototype_id:
				set_cell(0, tile, spawner.prototype_id, Vector2())

func update_spawners():
	

	if not is_instance_valid(get_tree()):
		return
	
	var cells = get_used_cells(0)
	
	for spawner in get_children():
		spawner.queue_free()
				
	var aux = (func(from_entity1, to_entity1): 
		for tile in cells.slice(from_entity1, min(to_entity1, cells.size())):
			var id = get_cell_source_id(0, tile)
					
			var spawner = spawner_scene.instantiate()
			if tile.y % 2 == 0:
				spawner.tile = Vector2i(tile.x*2, tile.y+1)
			else:
				spawner.tile = Vector2i(tile.x*2+1, tile.y+1)
			
			spawner.position = MapConstants.hex_to_position(spawner.tile)
			spawner.prototype_id = id
			
			var entity = entity_scenes[id].instantiate()
			var show_2d = show_2d_scene.instantiate()
			
			spawner.add_child(entity)
			spawner.add_child(show_2d)
			
			if get_parent():
				add_child.bind(spawner).call_deferred()
				spawner.set_owner.bind(get_tree().get_edited_scene_root()).call_deferred()
				entity.set_owner.bind(get_tree().get_edited_scene_root()).call_deferred()
				show_2d.set_owner.bind(get_tree().get_edited_scene_root()).call_deferred()
				(func(): spawner.name = entity.name + str(spawner.get_index())).call_deferred()
	)
	
	const SLICE = 40
	
	for i in range(int(float(cells.size())/SLICE) + 1):
		
		print(i*SLICE, " ", (i+1)*SLICE)
		aux.bind(i*SLICE, (i+1)*SLICE).call()
	
	
	
