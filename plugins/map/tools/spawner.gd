@tool
extends Node3D
class_name Spawner

@export var tile: Vector2i = Vector2i() : set = set_tile
@export var prototype_id := -1

signal tile_changed(tile: Vector2i)

var points = []
var point = Vector2()

func set_tile(tile_):
	tile = tile_
	
	if not is_inside_tree():
		return
	
	self.global_position = MapConstants.hex_to_position(tile)
	tile = MapConstants.position_to_hex(self.global_position)

	tile_changed.emit(tile)

	update_entity()

func _enter_tree():
	if !is_connected("child_entered_tree", spawn):
		self.connect("child_entered_tree", spawn)


func spawn(child):
	if child != get_child(0):
		return
	
	var radius = Vector2(0, -1) * MapConstants.L
	for i in range(6):
		points.append(radius)
		radius = radius.rotated(PI / 3)

	update_tile()

	if Engine.is_editor_hint():
		return

	if get_child(0):
		var entity = get_child(0)
		var hex = entity.get_node("Hex")
		remove_child.bind(entity).call_deferred()

		if hex:
			hex.position = self.global_position

		entity.propagate_call.bind("request_ready", []).call_deferred()
		add_sibling.bind(entity, true).call_deferred()

	queue_free.call_deferred()


func update_tile():
	tile = MapConstants.position_to_hex(self.global_position)
	point = MapConstants.hex_to_position(tile)
	self.global_position = point

	tile_changed.emit(tile)

	update_entity()


func update_entity():
	if get_child(0) == null:
		return
	var entity = get_child(0)
	var hex = entity.get_node("Hex")

	if hex:
		hex.position = self.global_position


func _process(_delta):
	if Engine.is_editor_hint():
		update_tile()


func push_warning2(a):
	push_warning(a)
