extends Node
class_name AreaAnimation

@export var inner_radius := 1
@export var outer_radius := 10
@export var color := Color.WHITE

# TODO: extract this
var jumping := false
var tiles3 = []

@onready var visual_map = Map.get_map().visual

func play(target: Vector2i):
	jumping = true
	var tiles2 = {}
	tiles3.push_back(tiles2)
	var timer
	
	for ring_radius in range(inner_radius+1, outer_radius+1):
		tiles2[ring_radius] = []
		for hex in MapConstants.get_hexes_on_ring(target, ring_radius):
			tiles2[ring_radius].push_back(
				visual_map.add_tile(hex, Color(color, 0.5))
			)
		
		timer = get_tree().create_timer(0.2)
		timer.timeout.connect((
			aux
		).bind(tiles2[ring_radius].duplicate()))
		
		await self.create_tween().tween_interval(0.025).finished
	
	if timer:
		await timer.timeout
	
	for tile in tiles2:
		AreaAnimation.aux(tile)
	
	jumping = false
		
# TODO: clean
static func aux(tiles):
	var visual_map_ = Map.get_map().visual
	
	for tile in tiles:
		visual_map_.remove_tile(tile)

func _exit_tree():
	if jumping:
		for tiles in tiles3:
			for tile in tiles:
				AreaAnimation.aux(tiles[tile])

