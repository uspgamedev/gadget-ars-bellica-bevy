extends Node2D
class_name VisualMap

var tiles = {}

var hex := Vector2i()
var current_id = 1


# TODO: Create interface for drawnig tiles
# TODO: Set canvas layer for tile
func add_tile(tile: Vector2i, color: Color, text := "", text_color := Color.RED) -> int:
	var id = current_id
	current_id += 1
	tiles[id] = [tile, color, text, text_color]
	queue_redraw()
	return id 
	
func remove_tile(id: int):
	queue_redraw()
	tiles.erase(id)
	

func _draw():
	var camera_tile = HexCamera.get_camera().get_hex()
	var camera_angle = HexCamera.get_camera().camera.rotation.y
	
	for tile_color in tiles.values():
		_draw_hex(tile_color[0] - camera_tile, tile_color[1])
		if not tile_color[2].is_empty():
			var point2 = MapConstants.hex_to_position(tile_color[0] - camera_tile) #.rotated(Vector3.UP, camera_angle)
			var point = Vector2(point2.x, point2.z)
			draw_set_transform(point, -camera_angle)
			draw_string(SystemFont.new(),
					 -Vector2(MapConstants.D / 2.0, -MapConstants.D / 4.0) - Vector2(0., MapConstants.D / 4.0).rotated(-camera_angle) + 
					Vector2(MapConstants.D / 2.0, 0.)*sin(camera_angle),
					 tile_color[2], HORIZONTAL_ALIGNMENT_CENTER, MapConstants.D, 32, tile_color[3])
			draw_set_transform(Vector2(), 0.)

func _draw_hex(tile: Vector2i, color: Color):
	var radius = Vector2(0, -1) * MapConstants.L

	var point2 = MapConstants.hex_to_position(tile) - Vector3(0, 0, MapConstants.L / 2.0)
	var point = Vector2(point2.x, point2.z)
	var points = []

	for k in range(6):
		points.append(radius.rotated(PI / 3 * k) + point)

	self.draw_colored_polygon(points, color)


#func add_tile_timer(tile: Vector2i, color: Color, interval: float):
	#pass
