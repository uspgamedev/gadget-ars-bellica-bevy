extends Node3D

@export var group_to_follow: String

var center_tile := Vector2i()

func _ready():
	start_following.call_deferred()


func start_following():
	var target = get_tree().get_first_node_in_group(group_to_follow)
	if target:
		var hex := target.get_component(Hex) as Hex
		hex.hex_changed.connect(follow)
		follow(hex.tile)

func follow(tile: Vector2i):
	if get_parent() is Node3D:
		get_parent().position = MapConstants.hex_to_position(tile)
		get_parent().position.y = 0
