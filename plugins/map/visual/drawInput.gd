extends Node2D

var desire_tile: Vector2i = Vector2i(2, 0)
	
func _draw():
	var radius = Vector2(0, -1) * MapConstants.L

	var point2 = MapConstants.hex_to_position(desire_tile) - Vector3(0, 0, MapConstants.L / 2.0)
	var point = Vector2(point2.x, point2.z)
	var points = []

	for k in range(6):
		points.append(radius.rotated(PI / 3 * k) + point)

	self.draw_colored_polygon(points, Color(0, 125, 0, 0.7))
