@tool
extends Node2D


func _draw():
	var radius = Vector2(0, -1) * MapConstants.L

	for ii in range(-50, 50, 1):
		for jj in range(-49, 50, 2):
			var i = ii
			var j = jj
			if i % 2:
				j += 1
			var point2 = (
				MapConstants.hex_to_position(Vector2i(i, j)) + Vector3(0, 0, MapConstants.L)
			)
			var point = Vector2(point2.x, point2.z)
			var points = []

			for k in range(6):
				points.append(radius.rotated(PI / 3 * k) + point)

			for k in 6:
				self.draw_line(points[k], points[(k + 1) % 6], Color(255, 255, 255, 0.5), 2.0)
