@tool
extends Node
class_name MapConst


const D := 60.0
const L := D / sqrt(3)
const DIRECTIONS : Array[Vector2i] = [Vector2i(2, 0), Vector2i(1, 1), Vector2i(-1, 1), 
					Vector2i(-2, 0), Vector2i(-1, -1), Vector2i(1, -1)]
					
#var CENTER_POSITION := Vector2(ProjectSettings.get("display/window/size/viewport_width")/2, ProjectSettings.get("display/window/size/viewport_width")/2)
const CENTER_POSITION := Vector2(540, 360)

#func normalize(hex: Vector2i) -> Vector2i:
#	if hex.y == 0:
#		return Vector2i(2, 0) if hex.x > 0 else Vector2i(-2, 0)
#
#	return hex / abs(hex.x)

func hex_to_position(hex: Vector2i) -> Vector3:
	var pos_2d = _hex_to_position(hex)
	return Vector3(pos_2d.x, 0, pos_2d.y)
	
func position_to_hex(position: Vector3) -> Vector2i:
	return _position_to_hex(Vector2(position.x, position.z))	
	
func _hex_to_position(hex: Vector2i) -> Vector2:
	return (hex as Vector2).floor() * Vector2(D/2, 3*L/2) + Vector2((0 if hex.y % 2 == 0 else 0), L/2)
	
func _position_to_hex(position: Vector2) -> Vector2i:
	
	var hex := Vector2i() 
	
	var get_hex_x = func(hex_y: int):
		return 2*floori((position.x)/D + (0.0 if hex_y%2 else 0.5))  + (1 if hex_y%2 else 0)
	
	hex.y = floori((2*position.y/L)/3)
	hex.x = get_hex_x.call(hex.y)

	var point = _hex_to_position(hex)
	var point2 =  -point+position
	
	if point2.y > L/2:
		if L + sqrt(3)/3 * point2.x < point2.y or L - sqrt(3)/3 * point2.x < point2.y:
			hex.y += 1
	if point2.y < -L/2:
		if -L - sqrt(3)/3 * point2.x < point2.y or -L + sqrt(3)/3 * point2.x < point2.y:
			hex.y -= 1
			
	
	hex.x = get_hex_x.call(hex.y)
	
	return hex

func hex_to_position_2d(hex: Vector2i) -> Vector2:
	return Vector2(hex.x * 8., hex.y * 12. - 4.)
	
func position_2d_to_hex(pos: Vector2) -> Vector2i:
	var tile = Vector2i(Vector2(pos.x / 8., (pos.y + 4) / 12.).round())
	if (tile.y % 2 and tile.x % 2 == 0) or (tile.y % 2 == 0 and tile.x % 2 == 1):
		if tile.x - pos.x / 8. < 0.5: 
			tile.x -= 1
		else:
			tile.x += 1
	
	return tile

func input_to_hex(input: Vector2i):
	var hex = input
	if input.length_squared() < 2:
		hex *= 2
	return hex

func get_hexes_on_area(hex: Vector2i, radius: int, inner_radius := 0) -> Array[Vector2i]:
	if radius <= 0 or radius == inner_radius:
		return [] as Array[Vector2i]
		
	if radius == 1:
		return [hex] as Array[Vector2i]
		
	var result := [] as Array[Vector2i]

	result.append_array(get_hexes_on_area(hex, radius-1, inner_radius))
	result.append_array(get_hexes_on_ring(hex, radius))
	
	return result
	
func get_hexes_on_ring(hex: Vector2i, radius: int) -> Array[Vector2i]:
	if radius <= 0:
		return [] as Array[Vector2i]
		
	if radius == 1:
		return [hex] as Array[Vector2i]
		
	var result := [] as Array[Vector2i]
	
	var true_radius = (radius-1)
	
	var current_hex = hex + true_radius * MapConstants.DIRECTIONS[4]
	
	for direction in MapConstants.DIRECTIONS:
		for i in true_radius:
			current_hex += direction
			result.append(current_hex)
	
	return result

func length(hex: Vector2i) -> float:
	for radius in 10000:
		if get_hexes_on_ring(Vector2i(), radius).find(hex) != -1:
			return radius-1
			
	push_warning("Invalid hex")
	return INF
		
func get_direction(vec: Vector2) -> Vector2i:
	var min_diff = -1
	var result := Vector2i()
	vec = vec.normalized()
	
	if vec.is_zero_approx():
		return Vector2i()
	
	for direction in DIRECTIONS:
		var diff = (vec - Vector2(direction).normalized()).length_squared()
		if min_diff == -1 or diff < min_diff:
			min_diff = diff
			result = direction
	
	return result
