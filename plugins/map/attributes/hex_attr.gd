extends Attribute
class_name HexAttr

@export var value := Vector2i()

func _init(hex := Vector2i()):
	value = hex
