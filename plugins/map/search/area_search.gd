extends SearchStrategy
class_name AreaSearchStrategy

@export var search_range := 1


func search(map: Map, tile: Vector2i) -> Array[Entity]:
	var result: Array[Entity] = []
	var tiles = [tile]

	for i in search_range:
		while !tiles.is_empty():
			var current_tile = tiles.pop_front()
			result.push_back(map.get_entity_on_hex(current_tile))

			for direction in MapConstants.DIRECTIONS:
				var new_tile = current_tile + direction
				tiles.push_back(new_tile)

	return result
