extends SearchStrategy
class_name SingleSearchStrategy


func search(map: Map, tile: Vector2i) -> Array:
	return map.get_entities_on_hex(tile)
