extends BodyAnimation	

var initial_position := 0

var tween: Tween

func _play():
	initial_position = body.sprite.offset.y
	
	tween = self.create_tween()
	tween.tween_property(body.sprite, "offset:y", initial_position + 40.0, 0.15).set_trans(Tween.TRANS_SPRING)
	tween.finished.connect(
		func():
			tween = self.create_tween()
			tween.tween_property(body.sprite, "offset:y", initial_position, 0.15).set_trans(Tween.TRANS_SPRING)
			tween.finished.connect(func(): finished.emit())
	)

func stop():
	if tween:
		tween.kill()
		tween = null
		body.sprite.offset.y = initial_position

