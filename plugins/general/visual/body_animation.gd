extends Node
class_name BodyAnimation

@onready var body: Body = EntityUtil.get_entity(self).get_component(Body)

signal finished()

func play():
	body.set_body_animation(self)
	_play()

func _play():
	push_error("Virtual method")
	pass
	
func stop():
	pass
