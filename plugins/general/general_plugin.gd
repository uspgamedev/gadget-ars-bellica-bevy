extends RefCounted
class_name GeneralPlugin


static func new_advance_turn_action() -> Action:
	return Action.new_action([TurnAttr.new()], "AdvanceTurn")
