extends Attribute
class_name FromAttr

var entity: Entity

func _init(_entity: Entity = null):
	self.entity = _entity

func _duplicate():
	return FromAttr.new(entity)
