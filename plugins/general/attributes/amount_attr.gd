extends Attribute
class_name AmountAttr

var value := 0.0

func _init(amount := 0.0):
	self.value = amount

func _duplicate():
	return AmountAttr.new(self.value)
