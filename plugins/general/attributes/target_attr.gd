extends Attribute
class_name TargetAttr

var entity: Entity

func _init(_entity: Entity = null):
	self.entity = _entity

func _duplicate():
	return TargetAttr.new(self.entity)
