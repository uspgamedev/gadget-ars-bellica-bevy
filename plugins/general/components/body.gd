@tool
extends Component3D
class_name Body

@export var sprite_frames : SpriteFrames : set = set_frames#, get = get_frames
@export var animation : StringName : set = set_animation#, get = get_animation
@export var icon : Texture2D
@export var offset := Vector2()
@export_enum("right", "left") var sprite_direction := "right"

const FADING_DURATION := 0.5

@onready var sprite := $Sprite
@onready var info1 := $Info1
@onready var info2 := $Info2

func _ready():
	var hex := get_entity().get_component(Hex) as Hex
	if hex:
		hex.start_moving.connect(func(direction: Vector2i):
			var ang1 = Vector2(direction).angle()
			var ang2 = -get_viewport().get_camera_3d().rotation.y
			
			# TODO: update flip on camera update
			if Vector2.from_angle(ang2-ang1).x < 0.0:
				$Sprite.flip_h = sprite_direction == "right"
			if Vector2.from_angle(ang2-ang1).x > 0.0:
				$Sprite.flip_h = sprite_direction == "left"
				
			if $Sprite.sprite_frames.get_animation_names().has("walk") and $Sprite.animation != "walk":
				
				$Sprite.play("walk")
		)
		hex.stop_moving.connect(func():
			if $Sprite.sprite_frames.get_animation_names().has("default"):
				$Sprite.play("default")
		)
	if sprite_direction == "left":
		$Sprite.flip_h = true

@onready var previous_modulate : Color = sprite.modulate
var fading_tween : Tween = null

func set_modulate(color: Color, fading := false):
	$Sprite.modulate = color
	if fading:
		if fading_tween:
			fading_tween.kill()
		fading_tween = create_tween()
		fading_tween.tween_property($Sprite, "modulate", previous_modulate, FADING_DURATION)
		fading_tween.finished.connect(func(): fading_tween = null)
	else:
		previous_modulate = color

func set_frames(frames):
	sprite_frames = frames
	var callable = (func():
		if frames and frames.get_frame_texture("default", 0):
			$Sprite.offset.x = offset.x - frames.get_frame_texture("default", 0).get_width()/2.0
			$Sprite.offset.y = offset.y 
		$Sprite.sprite_frames = frames
	)
	if is_node_ready():
		callable.call()
	else:
		ready.connect(callable)
	
func set_animation(new_animation):
	animation = new_animation
	var callable = (func():
		if $Sprite.sprite_frames and $Sprite.sprite_frames.has_animation(new_animation):
			$Sprite.animation = animation
	)
	if is_node_ready():
		callable.call()
	else:
		ready.connect(callable)

func play(animation_name: String):
	$Sprite.play(animation_name)

var current_animation: BodyAnimation

func set_body_animation(new_animation: BodyAnimation):
	if is_instance_valid(current_animation):
		current_animation.stop()
	current_animation = new_animation
