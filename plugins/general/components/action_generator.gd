extends Component

@export var action_attributes: Array[Attribute]

@export var period: float = 2.0


func _ready():
	$Timer.start(period)


func _on_timer_timeout():
	Simulation.apply_action(Action.new_action(action_attributes, ""))


func reset():
	if !$Timer.paused:
		$Timer.start(period)
