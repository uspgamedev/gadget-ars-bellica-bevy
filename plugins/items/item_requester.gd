extends Node
class_name ItemRequester

@export var node: Node

func request() -> Item:
	return node.as_item()
