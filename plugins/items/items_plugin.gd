extends Node
class_name ItemPlugin


static func new_inventory_action() -> Action:
	return Action.new_action([InventoryAttr.new()], "Inventory Action")
