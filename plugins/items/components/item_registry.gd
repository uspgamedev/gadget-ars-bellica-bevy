@tool
extends Component
#class_name ItemRegisstry

var registered_item_prototypes = {}

func register_item_prototypes(item_prototypes: Dictionary):
	print("Registering: ", item_prototypes)
	for item_id in item_prototypes:
		if registered_item_prototypes.has(item_id):
			push_error("Item prottoype already registered (Item ID:", item_id, " )")
			continue
		var prototype = item_prototypes[item_id]
		registered_item_prototypes[item_id] = prototype


func new_item_dict(item_id: String, custom_properties: Dictionary) -> Dictionary:
	if not registered_item_prototypes.has(item_id):
		push_error("Item ID NOT registered (Item ID:", item_id, " )")
		return {}

	var item_dict : Dictionary = registered_item_prototypes[item_id].duplicate()
	item_dict.merge(custom_properties, true)

	return item_dict

