extends Resource
class_name Item

@export var item_id := ""
@export var custom_properties := {}

func _init(_item_id := "", _custom_properties := {}):
	item_id = _item_id 
	custom_properties = _custom_properties

static func get_item_dict(item: Item) -> Dictionary:
	return ItemRegistry.new_item_dict(item.item_id, item.custom_properties)
