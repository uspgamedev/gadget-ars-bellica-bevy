extends Rule

var inventory_attr := InventoryAttr.new()

func _apply(simulation: Simulation):
	var items := simulation.query([DroppedItem])
	var map := Map.get_map()
	
	for item in items:
		#var entity = map.get_blocking_entity_on_hex(item.get_component(Hex).tile)
		# TODO: Check if entity has inventory and try apply an action for every entity 
		var entities = map.get_entities_on_hex(item.get_component(Hex).tile)\
				.filter(Blocking.filter_by_level.bind([Blocking.Level.Ground, Blocking.Level.Air]))
		
		for entity in entities:
			var pickup = PickUpAtrr.new()
			pickup.item = item.get_component(DroppedItem).item
			(func():
				if Simulation.apply_action(Action.new_action([pickup, TargetAttr.new(entity)], "PickUp Action")):
					Simulation.apply_action(CombatPlugin.new_kill_action(TargetAttr.new(item)))
			).call_deferred()
			
		
#		var entities_on_hex = map.get_entities_on_hex(item.get_component(Hex).tile).filter(
#			func(e): return e.get_component(Hand) != null
#		)
#
#
#
#		if not entities_on_hex.is_empty():
#			entity = entities_on_hex[0] as Entity
#			var i := item.get_component(DroppedItem) as DroppedItem
#			var item_dict = Item.get_item_dict(i.item)
#
#			if item_dict.has("grabbable") and entity.get_component(Hand) and entity.get_component(Hand).get_child_count() == 0:
#				var scene = item_dict["grabbable"]["scene"]
#				entity.get_component(Hand).add_child(load(scene).instantiate())
#				Simulation.apply_action(CombatPlugin.new_kill_action(TargetAttr.new(item)))
#			elif item_dict.has("modifier"):
#				var scene = item_dict["modifier"]["scene"]
#				if entity.get_component(Hand).get_child(0):
#					entity.get_component(Hand).get_child(0).add_child(load(scene).instantiate())
#					Simulation.apply_action(CombatPlugin.new_kill_action(TargetAttr.new(item)))
#			else:
#				var inventory: Inventory = entity.get_component(Inventory)
#
#				if inventory:
#					inventory.put_item(i.item)
#					Simulation.apply_action(CombatPlugin.new_kill_action(TargetAttr.new(item)))
#				pass
