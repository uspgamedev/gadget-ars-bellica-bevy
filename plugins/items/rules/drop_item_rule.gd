extends Rule

@export var dropped_item_scene: PackedScene

var target := TargetAttr.new()
var kill := KillAttr.new()

func _apply(simulation: Simulation):
	var drop_item = target.entity.get_component(DropItem)
	
	if drop_item:
		for item in target.entity.get_component(DropItem).items:
			var dropped_item = dropped_item_scene.instantiate()
			dropped_item.item = item
			dropped_item.get_node("Hex").position = target.entity.get_component(Hex).position
			simulation.add_child.bind(dropped_item).call_deferred()
		(func(): Simulation.apply_action(ItemPlugin.new_inventory_action())).call_deferred()
		
