@tool
extends Entity

@export var item: Item


func _ready():
	$DroppedItem.item = item
	if Item.get_item_dict(item).has("icon"):
		var icon = load(Item.get_item_dict(item)["icon"])
		$Body.icon = icon
		$Body.sprite_frames = SpriteFrames.new()
		$Body.sprite_frames.add_frame("default", icon)
