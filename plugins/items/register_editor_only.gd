@tool
extends Node

@export var item_prototypes_to_register : Array[JSON] = []

func _ready():
	if Engine.is_editor_hint():
		for prototypes in item_prototypes_to_register:
			get_parent().register_item_prototypes(prototypes.data)

