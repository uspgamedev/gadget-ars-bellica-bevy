extends Node3D

var spider_scene := preload("res://game/enemies/spider/Spider.tscn")

func _ready():
	var i = 0
	for hex in MapConstants.get_hexes_on_ring(Vector2i(25, -10), 7):
		i += 1
		if i % 3 != 0:
			continue
		var spider = spider_scene.instantiate()
		spider.get_component(Hex).tile = hex
		Simulation.get_simulation().add_child.bind(spider).call_deferred()

